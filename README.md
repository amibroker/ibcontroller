# IBController

IBController is AmiBroker's auto-trading interface for Interactive Brokers

## How to build

Use Microsoft Visual C++ 6.0 and open project workspace (BrokerIB.dsw)
or Microsoft Visual C++ 2017 and open solution file (BrokerIB.sln)

Then Build the project as usual

## How to use

Detailed instructions on how to use the software are available at: http://www.amibroker.com/at/