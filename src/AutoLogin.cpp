// AutoLogin.cpp: implementation of the CAutoLogin class.
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.

#include "stdafx.h"
#include "BrokerIB.h"
#include "AutoLogin.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAutoLogin::CAutoLogin()
{

}

CAutoLogin::~CAutoLogin()
{

}

UINT CAutoLogin::DoLogin(void *pThreadData)
{
	for( int i = 0; i < 8; i++ )
	{
		Sleep( 500 );
		HWND hWnd = FindWindow( "SunAwtDialog", "Trader Workstation" );

		if( hWnd == NULL )
		{
			hWnd = FindWindow( "SunAwtDialog", "IB Trader Workstation" );
		}

		if( hWnd == NULL )
		{
			hWnd = FindWindow( "SunAwtDialog", "Interactive Brokers Trader Workstation" );
		}

		if( hWnd )
		{
			PostMessage( hWnd, WM_KEYDOWN, VK_RETURN, 0 );
			Sleep( 20 );
			PostMessage( hWnd, WM_KEYUP, VK_RETURN, 0xC0000000 );
			return 1;
		}
	}
		
	return 0;
}
