// AutoLogin.h: interface for the CAutoLogin class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUTOLOGIN_H__7F571729_7A6E_4D28_B9E6_765622F68AFE__INCLUDED_)
#define AFX_AUTOLOGIN_H__7F571729_7A6E_4D28_B9E6_765622F68AFE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAutoLogin  
{
public:
	static UINT DoLogin( void *pThreadData );
	CAutoLogin();
	virtual ~CAutoLogin();

};

#endif // !defined(AFX_AUTOLOGIN_H__7F571729_7A6E_4D28_B9E6_765622F68AFE__INCLUDED_)
