// BaseListCtrl.cpp : implementation file
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "BaseListCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBaseListCtrl

CBaseListCtrl::CBaseListCtrl()
{
}

CBaseListCtrl::~CBaseListCtrl()
{
}


BEGIN_MESSAGE_MAP(CBaseListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CBaseListCtrl)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBaseListCtrl message handlers



void CBaseListCtrl::CreateAndInit(int nID, CRect &oRect, LPCTSTR pszColumns, CWnd *pParent)
{
	CWnd::CreateEx( WS_EX_CLIENTEDGE, "SysListView32", "", WS_CHILD | LVS_REPORT , oRect, pParent, nID );

	SetExtendedStyle( LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_HEADERDRAGDROP );	

	CString oColName;

	for( int iCol = 0; AfxExtractSubString( oColName, pszColumns, iCol, ','); iCol++ )
	{
		InsertColumn( iCol, oColName, LVCFMT_LEFT, 100 );
	}

	LoadLayout();
}





void CBaseListCtrl::LoadLayout()
{
	int nColQty = GetHeaderCtrl()->GetItemCount();

	int *anColWidth = NULL;
	int *anColOrder = NULL;

	int iColWidthSize;
	int iColOrderSize;

	CString oKey = GetLayoutKey();
	BOOL bSuccess = FALSE;
	
	bSuccess = AfxGetApp()->GetProfileBinary( oKey, "Width", (LPBYTE *) &anColWidth, (UINT *)&iColWidthSize )
	           && 
			   AfxGetApp()->GetProfileBinary( oKey, "Order", (LPBYTE *) &anColOrder, (UINT *)&iColOrderSize );

	iColWidthSize /= sizeof( int );
	iColOrderSize /= sizeof( int );

	if( bSuccess )
	{
		for( int iCol = 0; iCol < nColQty && iCol < iColWidthSize; iCol++ )
		{
			SetColumnWidth( iCol, anColWidth[ iCol ] );
		}

		SetColumnOrderArray( min( nColQty, iColOrderSize ), anColOrder );
	}

	delete[] anColWidth;
	delete[] anColOrder;
}

void CBaseListCtrl::SaveLayout()
{
	int nColQty = GetHeaderCtrl()->GetItemCount();

	int *anColWidth = new int[ nColQty ];
	int *anColOrder = new int[ nColQty ];

	for( int iCol = 0; iCol < nColQty; iCol++ )
	{
		anColWidth[ iCol ] = GetColumnWidth( iCol );
	}

	GetColumnOrderArray( anColOrder, nColQty );

	CString oKey = GetLayoutKey();
	AfxGetApp()->WriteProfileBinary( oKey, "Width", (LPBYTE)anColWidth, nColQty * sizeof( int ) );
	AfxGetApp()->WriteProfileBinary( oKey, "Order", (LPBYTE)anColOrder, nColQty * sizeof( int ) );
	

	delete[] anColWidth;
	delete[] anColOrder;
}

CString CBaseListCtrl::GetLayoutKey()
{
	CString oKey;

	CString oParentID;
	GetParent()->GetWindowText( oParentID );

	oParentID.Replace( ' ', '_' );

	oKey.Format( "ListView_%s_%d", oParentID, GetDlgCtrlID() );

	return oKey;
}

void CBaseListCtrl::OnDestroy() 
{
	SaveLayout();

	CListCtrl::OnDestroy();
	

}
