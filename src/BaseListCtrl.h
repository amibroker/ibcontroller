#if !defined(AFX_BASELISTCTRL_H__118633D8_143E_48F3_96D4_7C1A705F6853__INCLUDED_)
#define AFX_BASELISTCTRL_H__118633D8_143E_48F3_96D4_7C1A705F6853__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BaseListCtrl.h : header file
//
enum { LIST_ORDER, LIST_EXECUTION, LIST_PORTFOLIO, LIST_ACCOUNT, LIST_MESSAGE, LIST_LAST };
/////////////////////////////////////////////////////////////////////////////
// CBaseListCtrl window

class CBaseListCtrl : public CListCtrl
{
// Construction
public:
	CBaseListCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaseListCtrl)
	public:
	//}}AFX_VIRTUAL

// Implementation
public:
	CString GetLayoutKey( void );
	void SaveLayout( void );
	void LoadLayout( void );
	void CreateAndInit( int nID, CRect &oRect, LPCTSTR pszColumns, CWnd *pParent );
	virtual ~CBaseListCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CBaseListCtrl)
	afx_msg void OnDestroy();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BASELISTCTRL_H__118633D8_143E_48F3_96D4_7C1A705F6853__INCLUDED_)
