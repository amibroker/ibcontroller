; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CBrokerIBDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "brokerib.h"
LastPage=0

ClassCount=13
Class1=CBaseListCtrl
Class2=CBrokerIBApp
Class3=CAboutDlg
Class4=CBrokerIBDlg
Class5=CBrokerIBDlgAutoProxy
Class6=CIBAccountList
Class7=CIBExecutionList
Class8=CIBMessagesList
Class9=CIBOrderList
Class10=CIBPortfolioList
Class11=CUnlockDlg

ResourceCount=7
Resource1=IDD_UNLOCK_DIALOG
Resource2=IDD_ABOUTBOX (English (U.S.))
Resource3=IDD_CONFIG_DIALOG
Resource4=IDD_IGNORE_DIALOG
Resource5=IDD_BROKERIB_DIALOG (English (U.S.))
Class12=CConfigDlg
Resource6=IDR_MAIN_MENU
Class13=CIgnoreListDlg
Resource7=IDR_MAIN_TOOLBAR

[CLS:CBaseListCtrl]
Type=0
BaseClass=CListCtrl
HeaderFile=BaseListCtrl.h
ImplementationFile=BaseListCtrl.cpp

[CLS:CBrokerIBApp]
Type=0
BaseClass=CWinApp
HeaderFile=BrokerIB.h
ImplementationFile=BrokerIB.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=BrokerIBDlg.cpp
ImplementationFile=BrokerIBDlg.cpp
LastObject=CAboutDlg

[CLS:CBrokerIBDlg]
Type=0
BaseClass=CDialog
HeaderFile=BrokerIBDlg.h
ImplementationFile=BrokerIBDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=ID_FILE_IGNORE_LIST

[CLS:CBrokerIBDlgAutoProxy]
Type=0
BaseClass=CCmdTarget
HeaderFile=DlgProxy.h
ImplementationFile=DlgProxy.cpp
Filter=N
VirtualFilter=C
LastObject=CBrokerIBDlgAutoProxy

[CLS:CIBAccountList]
Type=0
BaseClass=CBaseListCtrl
HeaderFile=IBAccountList.h
ImplementationFile=IBAccountList.cpp

[CLS:CIBExecutionList]
Type=0
BaseClass=CBaseListCtrl
HeaderFile=IBExecutionList.h
ImplementationFile=IBExecutionList.cpp

[CLS:CIBMessagesList]
Type=0
BaseClass=CBaseListCtrl
HeaderFile=IBMessagesList.h
ImplementationFile=IBMessagesList.cpp

[CLS:CIBOrderList]
Type=0
BaseClass=CBaseListCtrl
HeaderFile=IBOrderList.h
ImplementationFile=IBOrderList.cpp
Filter=W
VirtualFilter=FWC
LastObject=ID_FILE_RECONNECT

[CLS:CIBPortfolioList]
Type=0
BaseClass=CBaseListCtrl
HeaderFile=IBPortfolioList.h
ImplementationFile=IBPortfolioList.cpp
LastObject=CIBPortfolioList

[CLS:CUnlockDlg]
Type=0
BaseClass=CDialog
HeaderFile=UnlockDlg.h
ImplementationFile=UnlockDlg.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg

[DLG:IDD_BROKERIB_DIALOG]
Type=1
Class=CBrokerIBDlg

[DLG:IDD_UNLOCK_DIALOG]
Type=1
Class=CUnlockDlg
ControlCount=9
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352
Control4=IDC_NAME_EDIT,edit,1350631552
Control5=IDC_STATIC,static,1342308352
Control6=IDC_CODE_EDIT,edit,1350631552
Control7=IDC_CHECK1,button,1342251011
Control8=IDC_STATIC,static,1342308352
Control9=IDC_URL_STATIC,static,1342308352

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_BROKERIB_DIALOG (English (U.S.))]
Type=1
Class=?
ControlCount=1
Control1=IDC_MAIN_TAB,SysTabControl32,1342177280

[MNU:IDR_MAIN_MENU]
Type=1
Class=?
Command1=ID_ORDER_TRANSMIT
Command2=ID_ORDER_CANCEL
Command3=ID_FILE_ENTER_CODE
Command4=ID_FILE_CONFIG
Command5=ID_FILE_IGNORE_LIST
Command6=ID_FILE_RECONNECT
Command7=ID_APP_EXIT
CommandCount=7

[TB:IDR_MAIN_TOOLBAR]
Type=1
Class=?
Command1=ID_ORDER_TRANSMIT
Command2=ID_ORDER_CANCEL
Command3=ID_ORDER_ERROR_CANCEL
Command4=ID_MESSAGES_CLEAR
Command5=ID_WINDOW_ALWAYS_ON_TOP
Command6=ID_ORDER_PANIC
CommandCount=6

[DLG:IDD_CONFIG_DIALOG]
Type=1
Class=CConfigDlg
ControlCount=10
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_STATIC,static,1342308352
Control5=IDC_HOST_EDIT,edit,1350631552
Control6=IDC_STATIC,static,1342308352
Control7=IDC_PORT_EDIT,edit,1350639744
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1342308352
Control10=IDC_MAX_ERRORS_EDIT,edit,1350639744

[CLS:CConfigDlg]
Type=0
HeaderFile=ConfigDlg.h
ImplementationFile=ConfigDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_PORT_EDIT
VirtualFilter=dWC

[DLG:IDD_IGNORE_DIALOG]
Type=1
Class=CIgnoreListDlg
ControlCount=4
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_IGNORE_EDIT,edit,1350631556
Control4=IDC_STATIC,static,1342308352

[CLS:CIgnoreListDlg]
Type=0
HeaderFile=IgnoreListDlg.h
ImplementationFile=IgnoreListDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_FILE_IGNORE_LIST
VirtualFilter=dWC

