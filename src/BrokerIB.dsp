# Microsoft Developer Studio Project File - Name="BrokerIB" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=BrokerIB - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BrokerIB.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BrokerIB.mak" CFG="BrokerIB - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BrokerIB - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "BrokerIB - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BrokerIB - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "TWSAPI\Include\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x415 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x415 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /libpath:"TWSAPI\lib\\"
# Begin Special Build Tool
OutDir=.\Release
SOURCE="$(InputPath)"
PostBuild_Cmds=copy             $(OutDir)\BrokerIB.exe             E:\Work\Broker            	copy             $(OutDir)\BrokerIB.exe             F:\Amibroker\ 
# End Special Build Tool

!ELSEIF  "$(CFG)" == "BrokerIB - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "TWSAPI\Include\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x415 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x415 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"TWSAPI\lib\\"
# Begin Special Build Tool
OutDir=.\Debug
SOURCE="$(InputPath)"
PostBuild_Cmds=copy $(OutDir)\BrokerIB.exe E:\Work\Broker
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "BrokerIB - Win32 Release"
# Name "BrokerIB - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AutoLogin.cpp
# End Source File
# Begin Source File

SOURCE=.\BaseListCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\BrokerIB.cpp
# End Source File
# Begin Source File

SOURCE=.\BrokerIB.odl
# End Source File
# Begin Source File

SOURCE=.\BrokerIB.rc
# End Source File
# Begin Source File

SOURCE=.\BrokerIBDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.cpp
# End Source File
# Begin Source File

SOURCE=.\TWSAPI\Src\EClientSocket.cpp
# End Source File
# Begin Source File

SOURCE=.\HyprLink.cpp
# End Source File
# Begin Source File

SOURCE=.\IBAccountList.cpp
# End Source File
# Begin Source File

SOURCE=.\IBExecutionList.cpp
# End Source File
# Begin Source File

SOURCE=.\IBMessagesList.cpp
# End Source File
# Begin Source File

SOURCE=.\IBOrderList.cpp
# End Source File
# Begin Source File

SOURCE=.\IBPortfolioList.cpp
# End Source File
# Begin Source File

SOURCE=.\IgnoreListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TWSAPI\Src\MySocket.cpp
# End Source File
# Begin Source File

SOURCE=.\OrderData.cpp
# End Source File
# Begin Source File

SOURCE=.\StatLink.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SymbolData.cpp
# End Source File
# Begin Source File

SOURCE=.\UnlockDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Wrapper.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AutoLogin.h
# End Source File
# Begin Source File

SOURCE=.\BaseListCtrl.h
# End Source File
# Begin Source File

SOURCE=.\BrokerIB.h
# End Source File
# Begin Source File

SOURCE=.\BrokerIBDlg.h
# End Source File
# Begin Source File

SOURCE=.\ConfigDlg.h
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.h
# End Source File
# Begin Source File

SOURCE=.\HyprLink.h
# End Source File
# Begin Source File

SOURCE=.\IBAccountList.h
# End Source File
# Begin Source File

SOURCE=.\IBExecutionList.h
# End Source File
# Begin Source File

SOURCE=.\IBMessagesList.h
# End Source File
# Begin Source File

SOURCE=.\IBOrderList.h
# End Source File
# Begin Source File

SOURCE=.\IBPortfolioList.h
# End Source File
# Begin Source File

SOURCE=.\IgnoreListDlg.h
# End Source File
# Begin Source File

SOURCE=.\OrderData.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StatLink.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SymbolData.h
# End Source File
# Begin Source File

SOURCE=.\UnlockDlg.h
# End Source File
# Begin Source File

SOURCE=.\Wrapper.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\BrokerIB.ico
# End Source File
# Begin Source File

SOURCE=.\res\BrokerIB.rc2
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\BrokerIB.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
