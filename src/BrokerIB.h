// BrokerIB.h : main header file for the BROKERIB application
//

#if !defined(AFX_BROKERIB_H__E1A8048F_66E3_438E_A5FA_070CA47E2AF5__INCLUDED_)
#define AFX_BROKERIB_H__E1A8048F_66E3_438E_A5FA_070CA47E2AF5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBApp:
// See BrokerIB.cpp for the implementation of this class
//

class CBrokerIBApp : public CWinApp
{
public:
	CBrokerIBApp();
	virtual ~CBrokerIBApp();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBrokerIBApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CBrokerIBApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	HANDLE m_hMutex;

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BROKERIB_H__E1A8048F_66E3_438E_A5FA_070CA47E2AF5__INCLUDED_)
