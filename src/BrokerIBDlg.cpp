// BrokerIBDlg.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "BrokerIBDlg.h"
#include "DlgProxy.h"
#include "IBAccountList.h"
#include "IBExecutionList.h"
#include "IBMessagesList.h"
#include "IBOrderList.h"
#include "IBPortfolioList.h"
#include "ewrapper.h"
#include "eclient.h"
#include "eclientsocket.h"
#include "Wrapper.h"
#include "Autologin.h"
#include "DlgProxy.h"
#include <math.h>
#include <afxpriv.h>
#include "UnlockDlg.h"
#include "ConfigDlg.h"
#include "IgnoreListDlg.h"

#pragma warning( disable : 4996 )

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//////////////////////////////////////////////////
// GLOBALS 
//////////////////////////////////////////////////
CWrapper	g_oWrapper;
EClient     *g_pClient = NULL;


static int g_nTimerID = 0;



/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlg dialog

IMPLEMENT_DYNAMIC(CBrokerIBDlg, CDialog);

CBrokerIBDlg::CBrokerIBDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBrokerIBDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBrokerIBDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = NULL;
	m_iLastSel = 0;
	m_bCancelOrdersWithErrors = FALSE;
	m_bTransmitAllowed = IsRegistered();
}

CBrokerIBDlg::~CBrokerIBDlg()
{
	// If there is an automation proxy for this dialog, set
	//  its back pointer to this dialog to NULL, so it knows
	//  the dialog has been deleted.
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;

   	for( int iList = 0; iList < LIST_LAST; iList++ )
	{
		delete m_apoLists[ iList ];
	}


}

void CBrokerIBDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBrokerIBDlg)
	DDX_Control(pDX, IDC_MAIN_TAB, m_oMainTab);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBrokerIBDlg, CDialog)
	//{{AFX_MSG_MAP(CBrokerIBDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_MAIN_TAB, OnSelchangeMainTab)
	ON_WM_TIMER()
	ON_COMMAND(ID_ORDER_CANCEL, OnOrderCancel)
	ON_COMMAND(ID_ORDER_TRANSMIT, OnOrderTransmit)
	ON_COMMAND(ID_WINDOW_ALWAYS_ON_TOP, OnWindowAlwaysOnTop)
	ON_COMMAND(ID_MESSAGES_CLEAR, OnMessagesClear)
	ON_COMMAND(ID_ORDER_ERROR_CANCEL, OnOrderErrorCancel)
	ON_UPDATE_COMMAND_UI(ID_ORDER_ERROR_CANCEL, OnUpdateOrderErrorCancel)
	ON_COMMAND(ID_FILE_ENTER_CODE, OnFileEnterCode)
	ON_COMMAND(ID_ORDER_PANIC, OnOrderPanic)
	ON_COMMAND(ID_FILE_CONFIG, OnFileConfig)
	ON_COMMAND(ID_FILE_RECONNECT, OnFileReconnect)
	ON_COMMAND(ID_FILE_IGNORE_LIST, OnFileIgnoreList)
	//}}AFX_MSG_MAP
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipText)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlg message handlers

BOOL CBrokerIBDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.
	InitToolbar();

	if( ! m_bTransmitAllowed )
	{
		CString oCaption;
		GetWindowText( oCaption );
		SetWindowText( oCaption + " Automatic transmit DISABLED. Use File | Enter Unlock Code to enable");
	}

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	m_oMainTab.InsertItem( 0, "Pending orders" );
	m_oMainTab.InsertItem( 1, "Executions" );
	m_oMainTab.InsertItem( 2, "Portfolio" );
	m_oMainTab.InsertItem( 3, "Account information" );
	m_oMainTab.InsertItem( 4, "Messages" );
		   
	CRect oRect;

	m_oMainTab.GetClientRect( oRect );

	m_oMainTab.AdjustRect( FALSE, oRect );

	m_oMainTab.ClientToScreen( oRect );

	ScreenToClient( oRect );

	m_apoLists[ 0 ] = new CIBOrderList( oRect, this );
	m_apoLists[ 1 ] = new CIBExecutionList( oRect, this );
	m_apoLists[ 2 ] = new CIBPortfolioList( oRect, this );
	m_apoLists[ 3 ] = new CIBAccountList( oRect, this );
	m_apoLists[ 4 ] = new CIBMessagesList( oRect, this );

	m_apoLists[ 0 ]->SetWindowPos( &wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW  );
	
	AdjustControlSizes();

	Connect();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CBrokerIBDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBrokerIBDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CBrokerIBDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

// Automation servers should not exit when a user closes the UI
//  if a controller still holds on to one of its objects.  These
//  message handlers make sure that if the proxy is still in use,
//  then the UI is hidden but the dialog remains around if it
//  is dismissed.

void CBrokerIBDlg::OnClose() 
{
	if (CanExit())
		CDialog::OnClose();
}

void CBrokerIBDlg::OnOK() 
{
	if (CanExit())
		CDialog::OnOK();
}

void CBrokerIBDlg::OnCancel() 
{
	if (CanExit())
		CDialog::OnCancel();
}

BOOL CBrokerIBDlg::CanExit()
{
	// If the proxy object is still around, then the automation
	//  controller is still holding on to this application.  Leave
	//  the dialog around, but hide its UI.
	if (m_pAutoProxy != NULL)
	{
		//ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}

void CBrokerIBDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if( ::IsWindow( m_oMainTab.m_hWnd ) ) 
	{
		AdjustControlSizes();

	}

	
}

void CBrokerIBDlg::OnSelchangeMainTab(NMHDR* /*pNMHDR*/, LRESULT* pResult) 
{
	/* pNMHDR may be NULL !!! */

	m_apoLists[ m_oMainTab.GetCurSel() ]->SetWindowPos( &wndTop, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW  );
	m_apoLists[ m_iLastSel ]->ShowWindow( SW_HIDE );

	m_iLastSel = m_oMainTab.GetCurSel();
	if( m_iLastSel == 4 ) m_oMainTab.HighlightItem( m_iLastSel, FALSE );
		
	*pResult = 0;
}


BOOL CBrokerIBDlg::Connect()
{
	if( 1 )
	{

 		CString oHost = AfxGetApp()->GetProfileString("Settings", "Host", "127.0.0.1" );
		int iPort = AfxGetApp()->GetProfileInt("Settings", "Port", 7496 );

		CAutoLogin oAutoLogin;
		AfxBeginThread( oAutoLogin.DoLogin, NULL ); 

		g_oWrapper.ConnectLists( m_apoLists );

		if( g_pClient == NULL ) g_pClient = new EClientSocket( &g_oWrapper );;
		if( g_pClient->eConnect( oHost, iPort /*port */, 0 /*clientid*/) )
		{
			g_oWrapper.m_iConnected = CONN_INPROGRESS;

			//g_pClient->setServerLogLevel(5);
			//g_pClient->reqManagedAccts();

			g_pClient->reqIds( 10000 );

			//::SendMessage( g_oWrapper.m_hAmiWnd, WM_USER_STREAMING_UPDATE, 0, 0 );
			RequestAccountInfo();

		}
		else
		{
			g_oWrapper.m_iConnected = CONN_DISCONNECTED;
		}

		if( g_nTimerID == 0 ) 
		{
			g_nTimerID = SetTimer( 4321, 1000, NULL ); //::SetTimer( g_oWrapper.m_hAmiWnd, 4321, 1000, IBTimerProc ); 
		}

	}
	else
	{
		CIBMessagesList *poList = ((CIBMessagesList *)m_apoLists[ LIST_MESSAGE ]);

		poList->AddMessage( g_oWrapper.m_oLastError );

		m_oMainTab.SetCurSel( 4 );

		LRESULT dummy;
		OnSelchangeMainTab( NULL, &dummy );

	}

    return TRUE;
}

BOOL CBrokerIBDlg::Disconnect()
{
	if( g_pClient && g_oWrapper.m_iConnected )
	{
		KillTimer(  g_nTimerID ); 

		g_nTimerID = 0;

		/*
		int iQty = g_oWrapper.m_oSymbols.GetSize();

		for( int iCnt = 0; iCnt < iQty; iCnt++ )
		{
		   //g_pClient->cancelMktData( g_oWrapper.m_oSymbols[ iCnt ].GetRequestID() );
		}
		*/

		g_pClient->reqAccountUpdates( FALSE, g_oWrapper.m_oInfoAccount );
		g_pClient->eDisconnect();
		Sleep(100);
		//delete g_pClient;
		//g_pClient = NULL;
		g_oWrapper.m_iConnected = 0;
		//g_oWrapper.m_oSymbols.RemoveAll();

	}

	return TRUE;
}

BOOL CBrokerIBDlg::DestroyWindow() 
{
	Disconnect();


	return CDialog::DestroyWindow();
}

void CBrokerIBDlg::RequestAccountInfo()
{
	if( g_pClient && g_oWrapper.m_iConnected )
	{
		g_pClient->reqOpenOrders();
		g_pClient->reqAutoOpenOrders( TRUE );
		g_pClient->reqAccountUpdates( TRUE, g_oWrapper.m_oInfoAccount );

	}

}

void CBrokerIBDlg::OnTimer(UINT nIDEvent) 
{
	CDialog::OnTimer(nIDEvent);

	try
	{

		if( g_nTimerID && nIDEvent == g_nTimerID && g_pClient  )
		{
			//TRACE("TIMED checkMessages\n");

			//if( g_oWrapper.m_hAmiWnd )
			//	::SendMessage( g_oWrapper.m_hAmiWnd, WM_USER_STREAMING_UPDATE, 0, 0 );
			if( g_oWrapper.m_iConnected )
			{
				g_pClient->checkMessages();
			}

			if( g_oWrapper.m_iConnected >= CONN_OK )
			{
	
				static int iSecondCounter = 0;

				if( ( iSecondCounter++ % 2 ) == 0 )
				{
					g_pClient->reqAllOpenOrders();
					//g_pClient->reqOpenOrders();

					if( m_apoLists[ LIST_ORDER ] ) ((CIBOrderList *)m_apoLists[ LIST_ORDER ])->Cleanup(m_bCancelOrdersWithErrors);
					
				}
			}
			else
			{
				static int iDisconnectCounter = 1;
 				if( ( iDisconnectCounter++ % 5 ) == 0 )
				{
					Connect();
				}
			}
		}
	}
	catch( CException *e )
	{
		if( g_oWrapper.m_poMessagesList ) g_oWrapper.m_poMessagesList->AddMessage( "CException" );

		e->Delete();
	}
	catch( ... )
	{
		if( g_oWrapper.m_poMessagesList ) g_oWrapper.m_poMessagesList->AddMessage( "Other Exception" );
	}
}

long CBrokerIBDlg::PlaceOrder( long lOrderId, LPCTSTR pszTicker, LPCTSTR pszAction, LPCTSTR pszType, long iQuantity, double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, double dTickSize, LPCTSTR pszAttributes, long lParentID, LPCTSTR pszOCAGroup, int iOCAType, LPCTSTR pszFAParams, LPCTSTR pszAccount )
{
	Contract contract;

	g_oWrapper.TickerToContract( pszTicker, contract );

	Order order;
	//order.triggerMethod = 7;  // change for Benjamin Snyder
	CString oAttrs;

	// fill with defaults
	g_oWrapper.OrderToAttributes( order, oAttrs, FALSE );

	// check if pending and retrieve pending order values
	order.orderId = IsOrderPending( lOrderId, &order ) ? lOrderId : g_oWrapper.m_nLastOrderID++; 
	order.action = pszAction;
	order.totalQuantity = iQuantity;
	order.orderType = pszType;

	if( lParentID )	order.parentId = lParentID;

    order.hidden  = false;
	order.account = strlen( pszAccount ) ? pszAccount : g_oWrapper.m_oAccount;


	oAttrs = pszAttributes;

	// modify attrs only if param not empty
	if( ! oAttrs.IsEmpty() )
		g_oWrapper.OrderToAttributes( order, oAttrs, FALSE );


	// set oca group only if param not empty
	if( strlen( pszOCAGroup ) )
	{
		order.ocaGroup = pszOCAGroup;
	}

	// set oca type only if param not zero
	if( iOCAType )
	{
		order.ocaType = iOCAType;
	}

	if( strlen( pszFAParams ) )
	{
		g_oWrapper.OrderToFAParams( order, CString( pszFAParams ), FALSE );
	}
	else
	if( g_oWrapper.IsFAAccount( order.account ) )  // if pszFAParams not defined then use default for FA only
	{
		g_oWrapper.OrderToFAParams( order, CString("All;;AvailableEquity"), FALSE );
	}


	if( dTickSize <= 0 ) dTickSize = 0.0001;

	if( pszTimeInForce )
	{
		CString oPart1, oPart2;

		AfxExtractSubString( oPart1, pszTimeInForce, 0, ';' );
		AfxExtractSubString( oPart2, pszTimeInForce, 1, ';' );

		order.tif = oPart1;

		if( strnicmp( oPart1, "GTD", 3 ) == 0 )
		{
			order.tif = "GTD";
			order.goodTillDate = oPart1.Mid( 3 );
			order.goodTillDate.TrimLeft();
		}

		if( strnicmp( oPart2, "GAT", 3 ) == 0 )
		{
			order.goodAfterTime = oPart2.Mid( 3 );
			order.goodAfterTime.TrimLeft();
		}
	}
	else
	{
		if( order.tif.IsEmpty() ) order.tif = "DAY";
	}


	order.lmtPrice = dTickSize * floor( 0.5 + dLimitPrice / dTickSize );
	order.auxPrice = dTickSize * floor( 0.5 + dStopPrice / dTickSize );
	order.transmit = m_bTransmitAllowed ? (bool) bTransmit : 0;

//	TRACE("%.10lf\n", order.lmtPrice );

	if( m_apoLists[ LIST_ORDER ] ) 
	{
		((CIBOrderList *)m_apoLists[ LIST_ORDER ])->OpenOrder( order.orderId, contract, order );
		((CIBOrderList *)m_apoLists[ LIST_ORDER ])->OrderStatus( order.orderId, order.transmit ? "PreSubmitted" : "NotYetTransmitted", 0, order.totalQuantity, 0, 0, 0, 0, 0 );
	}

	if( order.transmit )
	{
		((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ])->OpenOrder( order.orderId, contract, order );
		((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ])->OrderStatus( order.orderId, "PreSubmitted", 0, order.totalQuantity, 0, 0, 0, 0, 0 );
	}

	g_pClient->placeOrder( order.orderId, contract, order ); 
	g_pClient->reqOpenOrders();

	return order.orderId;
}

void CBrokerIBDlg::OnOrderCancel() 
{
	if( m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
		
		POSITION hPos = poList->GetFirstSelectedItemPosition();

		while( hPos )
		{
			int iItem = poList->GetNextSelectedItem( hPos );

			g_pClient->cancelOrder( atoi( poList->GetItemText( iItem, 0 ) ) );


			CString oItemState = poList->GetItemText( iItem, 2 );

			if( oItemState == "NotYetTransmitted" || oItemState == "Error" )
				poList->SetItemText( iItem, 2, "Cancelled" );

		}
	}
}

void CBrokerIBDlg::OnOrderTransmit() 
{
	if( ! m_bTransmitAllowed )
	{
		AfxMessageBox("Transmit is currently disabled.\nYou would need to enter unlock code in\nFile->Enter Unlock Code to allow automatic transmission,\nor... transmit orders manually from TWS");
		return;
	}

	if( m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
		
		POSITION hPos = poList->GetFirstSelectedItemPosition();

		while( hPos )
		{
			int iItem = poList->GetNextSelectedItem( hPos );

			if( poList->GetOrderStatus( iItem ) == "NotYetTransmitted" )
			{
				Order		order;
				Contract	contract;

				poList->GetOrderAndContractFromItem( iItem, contract, order );

				order.transmit = true;

				g_pClient->placeOrder( order.orderId, contract, order );
				g_pClient->reqOpenOrders();
				
				if( m_apoLists[ LIST_EXECUTION ] ) 
				{
					((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ])->OpenOrder( order.orderId, contract, order );
				}
			}
		}
	}
}

BOOL CBrokerIBDlg::CancelOrder(long lOrderId)
{
	BOOL bFound = FALSE;

	g_pClient->cancelOrder( lOrderId );
	
	if( m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
	
		int iItem = poList->FindOrder( lOrderId );

		if( iItem >= 0 )
		{
			CString oStatus = poList->GetOrderStatus( iItem );
			if( oStatus == "NotYetTransmitted" || oStatus == "Error" || oStatus == "NoStatus" )
			{
				poList->OrderStatus( lOrderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0 );
			}

			bFound = TRUE;
		}
	}

	return bFound;
}

void CBrokerIBDlg::GetPendingOrderStatus(long lOrderID, BOOL bInclFilled, CString &oStatus )
{
	if( m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
	
		int iItem = poList->FindOrder( lOrderID );

		if( iItem >= 0 )
		{
			oStatus = poList->GetOrderStatus( iItem );
		}
		else
		if( bInclFilled )
		{
			if( m_apoLists[ LIST_EXECUTION ] )
			{
				CIBExecutionList *poList = ((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ]);
			
				int iItem = poList->FindOrder( lOrderID );

				if( iItem >= 0 )
				{
					oStatus = poList->GetOrderStatus( iItem );
				}
			}
		}
	}
}

BOOL CBrokerIBDlg::IsOrderPending(long lOrderID, Order *pOrder)
{
	if( lOrderID && m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
	
		int iItem = poList->FindOrder( lOrderID );

		if( iItem >= 0 )
		{
			CString oString;

			if( pOrder )
			{
				Contract contract;

				poList->GetOrderAndContractFromItem( iItem, contract, *pOrder );
			}

			oString = poList->GetOrderStatus( iItem ) ;

			return (oString != "Filled" && oString != "Cancelled" && oString != "NoStatus" && oString != "Error" );
		}
	}

	return FALSE;
}

int CBrokerIBDlg::GetPositionSize(LPCTSTR pszTicker)
{
	Contract contract;

	int iPosSize = 0;

	g_oWrapper.TickerToContract( pszTicker, contract );

	if( m_apoLists[ LIST_PORTFOLIO ] )
	{
		CIBPortfolioList *poList = ((CIBPortfolioList *)m_apoLists[ LIST_PORTFOLIO ]);

		int iItem = poList->FindContract( contract );

		if( iItem >= 0 )
		{
			iPosSize = poList->GetPositionSize( iItem );
		}
	}

	return iPosSize;
}

int CBrokerIBDlg::InitToolbar()
{
	// Add the ToolBar.
	if (!m_wndToolBar.Create( this ) ||
		!m_wndToolBar.LoadToolBar(IDR_MAIN_TOOLBAR) )
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	m_wndToolBar.SetButtonStyle( m_wndToolBar.CommandToIndex( ID_WINDOW_ALWAYS_ON_TOP ), TBBS_CHECKBOX ); 


	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY  );

	m_wndToolBar.SetButtonStyle( m_wndToolBar.CommandToIndex( ID_ORDER_ERROR_CANCEL ), TBBS_CHECKBOX );

// We need to resize the dialog to make room for control bars.
	// First, figure out how big the control bars are.
	
	// And position the control bars
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);



	return 0;
}

BOOL CBrokerIBDlg::OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult)
{
	ASSERT(pNMHDR->code == TTN_NEEDTEXTA || pNMHDR->code == TTN_NEEDTEXTW);

	// allow top level routing frame to handle the message
	if (GetRoutingFrame() != NULL)
		return FALSE;

	// need to handle both ANSI and UNICODE versions of the message
	TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
	TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
	TCHAR szFullText[256];
	CString cstTipText;
	CString cstStatusText;

	UINT nID = pNMHDR->idFrom;
	if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
		pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
	{
		// idFrom is actually the HWND of the tool
		nID = ((UINT)(WORD)::GetDlgCtrlID((HWND)nID));
	}

	if (nID != 0) // will be zero on a separator
	{
		AfxLoadString(nID, szFullText);
			// this is the command id, not the button index
		AfxExtractSubString(cstTipText, szFullText, 1, '\n');
		AfxExtractSubString(cstStatusText, szFullText, 0, '\n');
	}

	// Non-UNICODE Strings only are shown in the tooltip window...
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, cstTipText,
            (sizeof(pTTTA->szText)/sizeof(pTTTA->szText[0])));
	else
		_mbstowcsz(pTTTW->szText, cstTipText,
            (sizeof(pTTTW->szText)/sizeof(pTTTW->szText[0])));
	*pResult = 0;

	// bring the tooltip window above other popup windows
	::SetWindowPos(pNMHDR->hwndFrom, HWND_TOP, 0, 0, 0, 0,
		SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOMOVE);


	return TRUE;    // message was handled
}

void CBrokerIBDlg::AdjustControlSizes()
{
	CRect rcClientStart;
	CRect rcClientNow;
	GetClientRect(rcClientStart);
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 
			   0, reposQuery, rcClientNow);


	rcClientNow.DeflateRect( 5, 5, 5, 5 );

	m_oMainTab.MoveWindow( rcClientNow );

 	CRect oRect;

	m_oMainTab.GetClientRect( oRect );

	m_oMainTab.AdjustRect( FALSE, oRect );

	m_oMainTab.ClientToScreen( oRect );

	ScreenToClient( oRect );

	for( int iList = 0; iList < LIST_LAST; iList++ )
	{
		m_apoLists[ iList ]->MoveWindow( oRect );
	}

		// And position the control bars
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
}

void CBrokerIBDlg::OnWindowAlwaysOnTop() 
{
	BOOL bOnTop = m_wndToolBar.GetToolBarCtrl().IsButtonChecked(  ID_WINDOW_ALWAYS_ON_TOP );

	SetWindowPos( bOnTop ? &wndTopMost : &wndNoTopMost , 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE );
}

int CBrokerIBDlg::IsConnected()
{
	return g_oWrapper.m_iConnected;
}

void CBrokerIBDlg::OnMessagesClear() 
{
	if( m_apoLists[ LIST_MESSAGE ] )
	{
		CIBMessagesList *poList = ((CIBMessagesList *)m_apoLists[ LIST_MESSAGE ]);

		poList->DeleteAllItems();
		m_oMainTab.HighlightItem( 4, FALSE );
	}
}

void CBrokerIBDlg::OnOrderErrorCancel() 
{
	m_bCancelOrdersWithErrors = ! m_bCancelOrdersWithErrors;
}

void CBrokerIBDlg::OnUpdateOrderErrorCancel(CCmdUI* pCmdUI) 
{
	pCmdUI->SetCheck( m_bCancelOrdersWithErrors );
}

void CBrokerIBDlg::GetLastError(long lOrderId, CString &oErrorMsg)
{
	if( lOrderId > 0 )
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
	
		int iItem = poList->FindOrder( lOrderId );

		if( iItem >= 0 )
		{
			oErrorMsg = poList->GetOrderErrorMsg( iItem ) ;
		}
	}
	else
	{
		CIBMessagesList *poList = ((CIBMessagesList *)m_apoLists[ LIST_MESSAGE ]);

		if( poList->GetItemCount() > 0 )
		{
			oErrorMsg = poList->GetItemText( 0, 0 );
		}
	}

}

void CBrokerIBDlg::GetAccountValue(LPCTSTR pszKey, CString &value)
{
	((CIBAccountList *)m_apoLists[ LIST_ACCOUNT ])->GetValue( pszKey, value );
}

void CBrokerIBDlg::GetPositionInfo(LPCTSTR pszTicker, LPCTSTR pszField, VARIANT &result)
{
	Contract contract;

	int iPosSize = 0;

	g_oWrapper.TickerToContract( pszTicker, contract );

	if( m_apoLists[ LIST_PORTFOLIO ] )
	{
		CIBPortfolioList *poList = ((CIBPortfolioList *)m_apoLists[ LIST_PORTFOLIO ]);

		int iItem = poList->FindContract( contract );

		if( iItem >= 0 )
		{
			LVCOLUMN oColInfo;
			char buffer[ 256 ];

			oColInfo.mask = LVCF_TEXT;	
			oColInfo.pszText = buffer;
			oColInfo.cchTextMax = sizeof( buffer ) - 1;

			int iQty = poList->GetHeaderCtrl()->GetItemCount();

			for( int iCol = 0; poList->GetColumn( iCol, &oColInfo ) && iCol < iQty; iCol++ )
			{
				//OutputDebugString( CString("\npszText = ") + oColInfo.pszText );
				if( stricmp( oColInfo.pszText, pszField ) == 0 )
				{
					result.vt = VT_R8;
					result.dblVal = atof( poList->GetItemText( iItem, iCol ) );

					return;
				}
			}
		}
	}

}

void CBrokerIBDlg::GetExecInfo(int lOrderID, LPCTSTR pszField, VARIANT &result)
{
	if( m_apoLists[ LIST_EXECUTION ] )
	{
		CIBExecutionList *poList = ((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ]);

		int iItem = poList->FindOrder( lOrderID );

		if( iItem >= 0 )
		{
			LVCOLUMN oColInfo;
			char buffer[ 256 ];

			oColInfo.mask = LVCF_TEXT;	
			oColInfo.pszText = buffer;
			oColInfo.cchTextMax = sizeof( buffer ) - 1;

			int iQty = poList->GetHeaderCtrl()->GetItemCount();

			for( int iCol = 0; poList->GetColumn( iCol, &oColInfo ) && iCol < iQty; iCol++ )
			{
				//OutputDebugString( CString("\npszText = ") + oColInfo.pszText );
				if( stricmp( oColInfo.pszText, pszField ) == 0 )
				{
					CString oItemText = poList->GetItemText( iItem, iCol );

					if( iCol != 0 && 
						! oItemText.IsEmpty() && 
						! isdigit( oItemText[ 0 ] ) )
					{
						result = COleVariant( oItemText );
						return;
					}


					result.vt = VT_R8;
					result.dblVal = atof( oItemText );

					return;
				}
			}
		}
	}

}

void CBrokerIBDlg::OnFileEnterCode() 
{
	CUnlockDlg oDlg;
	oDlg.DoModal();

	m_bTransmitAllowed = IsRegistered();   

	if(	!m_bTransmitAllowed )
		SetWindowText( "IB Controller. Automatic transmit DISABLED. Use File | Enter Unlock Code to enable");
	else
		SetWindowText( "IB Controller");


}



int CBrokerIBDlg::GetPositionList(CString &oString)
{
	int iQty = 0;

	if( m_apoLists[ LIST_PORTFOLIO ] )
	{
		CIBPortfolioList *poList = ((CIBPortfolioList *)m_apoLists[ LIST_PORTFOLIO ]);

		iQty = poList->GetItemCount();

		for( int iItem = 0; iItem < iQty; iItem++ )
		{
			CString oCurrent = poList->GetPositionSymbol( iItem );

			if( ! oString.IsEmpty() ) oString += ",";

			oString += oCurrent;
		}
	}

	return iQty;
}

int CBrokerIBDlg::GetPendingList(CString &oResult, int iType, LPCTSTR pszFilter)
{
	int iQty = 0;

	if( m_apoLists[ LIST_ORDER ] )
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);

		iQty = poList->GetItemCount();

		BOOL bApplyFilter = strlen( pszFilter ) > 0;

		for( int iItem = 0; iItem < iQty; iItem++ )
		{
			if( ! bApplyFilter ||
				poList->GetOrderStatus( iItem ).CompareNoCase( pszFilter ) == 0 )
			{
				Contract ct;
				Order ord;

				poList->GetOrderAndContractFromItem( iItem, ct, ord );

				if( ! oResult.IsEmpty() ) oResult += ",";

				if( iType == 0 ) // order id
				{
					CString oOrderStr;
					oOrderStr.Format("%d", ord.orderId ) ;

					oResult += oOrderStr;
				}
				else
				{
					oResult += g_oWrapper.ContractToTicker( ct );
				}
			}

		}
	}

	return iQty;
}


int CBrokerIBDlg::GetExecList(CString &oResult, int iType, LPCTSTR pszFilter)
{
	int iQty = 0;

	if( m_apoLists[ LIST_EXECUTION ] )
	{
		CIBExecutionList *poList = ((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ]);

		iQty = poList->GetItemCount();

		BOOL bApplyFilter = strlen( pszFilter ) > 0;

		for( int iItem = 0; iItem < iQty; iItem++ )
		{
			if( ! bApplyFilter ||
				poList->GetOrderStatus( iItem ).CompareNoCase( pszFilter ) == 0 )
			{
			//	Contract ct;
			//	Order ord;

			//	poList->GetOrderAndContractFromItem( iItem, ct, ord );

				if( ! oResult.IsEmpty() ) oResult += ",";

				if( iType == 0 ) // order id
				{
					CString oOrderStr;
					oOrderStr.Format("%d", poList->GetOrderID( iItem ) ) ;

					oResult += oOrderStr;
				}
				else
				{
			//		oResult += g_oWrapper.ContractToTicker( ct );
				}
			}

		}
	}

	return iQty;
}

int CBrokerIBDlg::CancelAllPendingOrders( LPCTSTR pszTicker )
{
	int iQty = 0;

	// if ticker is not null but empty, null the pointer (simplifies further code)
	if( pszTicker && strlen( pszTicker ) == 0 ) pszTicker = NULL;

	Contract onlythis;

	if( pszTicker )
	{
		g_oWrapper.TickerToContract( pszTicker, onlythis );
	}

	if( m_apoLists[ LIST_ORDER ] )
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);

		iQty = poList->GetItemCount();

		for( int iItem = iQty - 1; iItem >= 0; iItem-- )
		{
			CString oStatus = poList->GetOrderStatus( iItem );
			
			if( oStatus != "Cancelled" )
			{
				Contract ct;
				Order ord;

				poList->GetOrderAndContractFromItem( iItem, ct, ord );

				if( pszTicker == NULL ||
					( ( ct.symbol == onlythis.symbol || ct.localSymbol == onlythis.localSymbol ) &&
					  ct.secType == onlythis.secType &&
					  ct.exchange == onlythis.exchange ) 
				  )
				{
					CancelOrder( ord.orderId );
				}
			}
		}
	}

	return iQty;

}

int CBrokerIBDlg::CloseAllOpenPositions( LPCTSTR pszTicker )
{
	int iQty = 0;

	// if ticker is not null but empty, null the pointer (simplifies further code)
	if( pszTicker && strlen( pszTicker ) == 0 ) pszTicker = NULL;

	CString oFullSymbol;

	if( pszTicker )
	{
		Contract onlythis;
		g_oWrapper.TickerToContract( pszTicker, onlythis );
		oFullSymbol = g_oWrapper.ContractToTicker( onlythis );
	}

	if( m_apoLists[ LIST_PORTFOLIO ] )
	{
		CIBPortfolioList *poList = ((CIBPortfolioList *)m_apoLists[ LIST_PORTFOLIO ]);

		iQty = poList->GetItemCount();

		for( int iItem = 0; iItem < iQty; iItem++ )
		{
			CString oCurrent = poList->GetPositionSymbol( iItem );
			int iSize = poList->GetPositionSize( iItem );

			CString oAccount = poList->GetPositionAccount( iItem );

			if( pszTicker == NULL ||
				oCurrent == oFullSymbol )
			{
				if( iSize != 0 )
				{
					PlaceOrder( 0, oCurrent, iSize > 0 ? "SELL" : "BUY", "MKT", abs( iSize ), 0, 0, "DAY", TRUE, 0, "", 0, "", 0, g_oWrapper.IsFAAccount(oAccount) ? "" : "All;;AvailableEquity", oAccount );
				}
			}
		}
	}

	return iQty;
}

void CBrokerIBDlg::OnOrderPanic() 
{

	if( AfxMessageBox("Do you really want to cancel all pending orders and close all open positions?", MB_YESNO ) == IDYES )
	{
		CancelAllPendingOrders();
		CloseAllOpenPositions();
	}
}

BOOL CBrokerIBDlg::Transmit(long lOrderID )
{
	if( ! m_bTransmitAllowed )
	{
		//AfxMessageBox("Transmit is currently disabled.\nYou would need to enter unlock code in\nFile->Enter Unlock Code to allow automatic transmission,\nor... transmit orders manually from TWS");
		return FALSE;
	}

	if( m_apoLists[ LIST_ORDER ] ) 
	{
		CIBOrderList *poList = ((CIBOrderList *)m_apoLists[ LIST_ORDER ]);
		
		int iItem = poList->FindOrder( lOrderID );


		if( iItem >= 0 )
		{
			CString oStatus = poList->GetOrderStatus( iItem );

			if( oStatus == "NotYetTransmitted" || 
				oStatus == "Error" )
			{
				Order		order;
				Contract	contract;

				poList->GetOrderAndContractFromItem( iItem, contract, order );

				order.transmit = true;

				g_pClient->placeOrder( order.orderId, contract, order );
				g_pClient->reqOpenOrders();
				
				if( m_apoLists[ LIST_EXECUTION ] ) 
				{
					((CIBExecutionList *)m_apoLists[ LIST_EXECUTION ])->OpenOrder( order.orderId, contract, order );
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CBrokerIBDlg::SetAccount(LPCTSTR pszAccount)
{
	g_oWrapper.m_oAccount = pszAccount;
	return TRUE;
}

void CBrokerIBDlg::ClearList(int iListNo)
{
	if( iListNo == -1 )	
	{
		for( int i = 0; i < LIST_LAST; i++ )
		{
			if( m_apoLists[ i ] ) m_apoLists[ i ]->DeleteAllItems();
		}
	}
	else
	if( iListNo >= 0 && iListNo < LIST_LAST )
	{
		m_apoLists[ iListNo ]->DeleteAllItems();
	}

	if( g_pClient && g_oWrapper.m_iConnected )
	{
		g_pClient->reqOpenOrders();
	}
}

void CBrokerIBDlg::OnFileConfig() 
{
	CConfigDlg oDlg;

	if( oDlg.DoModal() == IDOK )
	{
		if( oDlg.m_bConnectionChanged )
		{
			Disconnect();
			Connect();
		}

		if( m_apoLists[ LIST_MESSAGE ] ) 
		{
			((CIBMessagesList *)m_apoLists[ LIST_MESSAGE ])->UpdateItemLimit();
		}
	}
}

void CBrokerIBDlg::OnFileReconnect() 
{
	Disconnect();
	Connect();
	
}

void CBrokerIBDlg::OnFileIgnoreList() 
{
	CIgnoreListDlg oDlg;

	oDlg.m_pNumArray = &g_oWrapper.m_oIgnoreArray;

	oDlg.DoModal();
	
}

BOOL CBrokerIBDlg::SetInfoAccount(LPCTSTR pszAccount)
{
	g_pClient->reqAccountUpdates( FALSE, g_oWrapper.m_oInfoAccount );
//	g_pClient->reqAccountUpdates( TRUE, g_oWrapper.m_oInfoAccount );

	g_oWrapper.m_oInfoAccount = pszAccount;

	RequestAccountInfo();

	return TRUE;
}
