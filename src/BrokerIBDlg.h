// BrokerIBDlg.h : header file
//

#if !defined(AFX_BROKERIBDLG_H__368384CC_B741_47B0_A042_859435179648__INCLUDED_)
#define AFX_BROKERIBDLG_H__368384CC_B741_47B0_A042_859435179648__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CBrokerIBDlgAutoProxy;

#include "BaseListCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlg dialog
struct Order;


class CBrokerIBDlg : public CDialog
{
	DECLARE_DYNAMIC(CBrokerIBDlg);
	friend class CBrokerIBDlgAutoProxy;

// Construction
public:
	BOOL SetInfoAccount( LPCTSTR pszAccount );
	void ClearList( int iListNo );
	BOOL SetAccount( LPCTSTR pszAccount );
	BOOL Transmit( long lOrderID );
	int CloseAllOpenPositions( LPCTSTR pszTicker = NULL );
	int CancelAllPendingOrders( LPCTSTR pszTicker = NULL );
	int GetPendingList( CString& oResult, int iType, LPCTSTR pszFilter );
	int GetExecList( CString& oResult, int iType, LPCTSTR pszFilter );
	int GetPositionList(CString& oString );
	BOOL m_bTransmitAllowed;
	void GetExecInfo( int lOrderId, LPCTSTR pszField, VARIANT &result );
	void GetPositionInfo( LPCTSTR pszTicker, LPCTSTR pszField, VARIANT &result );
	void GetAccountValue( LPCTSTR pszKey, CString &value );
	void GetLastError( long lOrderId, CString& oErrorMsg );
	BOOL m_bCancelOrdersWithErrors;
	int IsConnected();
	void AdjustControlSizes( void );
	int InitToolbar();
	int GetPositionSize( LPCTSTR pszTicker );
	BOOL IsOrderPending( long lOrderID, Order *pOrder = NULL );
	void GetPendingOrderStatus( long lOrderID, BOOL bInclFilled, CString &oStatus );
	BOOL CancelOrder( long lOrderId );
	long PlaceOrder( long lOrderId, LPCTSTR pszTicker, LPCTSTR pszAction, LPCTSTR pszType, long iQuantity, double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, double dTickSize, LPCTSTR pszAttributes, long lParentID, LPCTSTR pszOCAGroup, int iOCAType, LPCTSTR pszFAParams, LPCTSTR pszAccount );
	void RequestAccountInfo( void );
	BOOL Disconnect( void );
	BOOL Connect( void );
	int m_iLastSel;
	CBrokerIBDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CBrokerIBDlg();

	CToolBar m_wndToolBar;

	CString m_oAccount;

// Dialog Data
	//{{AFX_DATA(CBrokerIBDlg)
	enum { IDD = IDD_BROKERIB_DIALOG };
	CTabCtrl	m_oMainTab;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBrokerIBDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CBrokerIBDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;


	CBaseListCtrl	*m_apoLists[ LIST_LAST ];

	BOOL CanExit();

	// Generated message map functions
	//{{AFX_MSG(CBrokerIBDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeMainTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnOrderCancel();
	afx_msg void OnOrderTransmit();
	afx_msg BOOL OnToolTipText(UINT, NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnWindowAlwaysOnTop();
	afx_msg void OnMessagesClear();
	afx_msg void OnOrderErrorCancel();
	afx_msg void OnUpdateOrderErrorCancel(CCmdUI* pCmdUI);
	afx_msg void OnFileEnterCode();
	afx_msg void OnOrderPanic();
	afx_msg void OnFileConfig();
	afx_msg void OnFileReconnect();
	afx_msg void OnFileIgnoreList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BROKERIBDLG_H__368384CC_B741_47B0_A042_859435179648__INCLUDED_)
