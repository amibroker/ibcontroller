// ConfigDlg.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "brokerib.h"
#include "ConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog


CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigDlg)
	m_oHost = _T("");
	m_iMaxErrors = 0;
	m_iPort = 0;
	//}}AFX_DATA_INIT
	m_oHost = AfxGetApp()->GetProfileString("Settings", "Host", "127.0.0.1" );
	m_iPort = AfxGetApp()->GetProfileInt("Settings", "Port", 7496 );
	m_iMaxErrors = AfxGetApp()->GetProfileInt("Settings", "MaxErrors", 0 );

	m_bConnectionChanged = FALSE;
}


void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Text(pDX, IDC_HOST_EDIT, m_oHost);
	DDX_Text(pDX, IDC_MAX_ERRORS_EDIT, m_iMaxErrors);
	DDX_Text(pDX, IDC_PORT_EDIT, m_iPort);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	ON_EN_CHANGE(IDC_HOST_EDIT, OnChangeConnection)
	ON_EN_CHANGE(IDC_PORT_EDIT, OnChangeConnection)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

void CConfigDlg::OnOK() 
{
	CDialog::OnOK();

	AfxGetApp()->WriteProfileString("Settings", "Host", m_oHost );
	AfxGetApp()->WriteProfileInt("Settings", "Port", m_iPort );
	AfxGetApp()->WriteProfileInt("Settings", "MaxErrors", m_iMaxErrors );


}

void CConfigDlg::OnChangeConnection() 
{
	m_bConnectionChanged = TRUE;
}
