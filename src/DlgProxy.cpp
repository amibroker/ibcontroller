// DlgProxy.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BrokerIB.h"
#include "DlgProxy.h"
#include "BrokerIBDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlgAutoProxy

IMPLEMENT_DYNCREATE(CBrokerIBDlgAutoProxy, CCmdTarget)

CBrokerIBDlgAutoProxy::CBrokerIBDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CBrokerIBDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (CBrokerIBDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

CBrokerIBDlgAutoProxy::~CBrokerIBDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CBrokerIBDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CBrokerIBDlgAutoProxy, CCmdTarget)
	//{{AFX_MSG_MAP(CBrokerIBDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CBrokerIBDlgAutoProxy, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CBrokerIBDlgAutoProxy)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "PlaceOrder", PlaceOrder, VT_BSTR, VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_R8 VTS_R8 VTS_BSTR VTS_BOOL VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "ModifyOrder", ModifyOrder, VT_BSTR, VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_R8 VTS_R8 VTS_BSTR VTS_BOOL VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "CancelOrder", CancelOrder, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetStatus", GetStatus, VT_BSTR, VTS_BSTR VTS_VARIANT)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "IsOrderPending", IsOrderPending, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetPositionSize", GetPositionSize, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "IsConnected", IsConnected, VT_I4, VTS_NONE)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetLastError", GetLastError, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetAccountValue", GetAccountValue, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetPositionInfo", GetPositionInfo, VT_VARIANT, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetPositionList", GetPositionList, VT_BSTR, VTS_NONE)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetPendingList", GetPendingList, VT_BSTR, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "CancelAllPendingOrders", CancelAllPendingOrders, VT_I4, VTS_VARIANT)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "CloseAllOpenPositions", CloseAllOpenPositions, VT_I4, VTS_VARIANT)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "Transmit", Transmit, VT_I4, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "Sleep", Sleep, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "SetAccount", SetAccount, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "ClearList", ClearList, VT_EMPTY, VTS_I4)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "Reconnect", Reconnect, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetExecList", GetExecList, VT_BSTR, VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "GetExecInfo", GetExecInfo, VT_VARIANT, VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CBrokerIBDlgAutoProxy, "SetInfoAccount", SetInfoAccount, VT_BOOL, VTS_BSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IBrokerIB to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {AE10BA16-3ABF-4BBC-A0D3-23D1613A3480}
static const IID IID_IBrokerIB =
{ 0xae10ba16, 0x3abf, 0x4bbc, { 0xa0, 0xd3, 0x23, 0xd1, 0x61, 0x3a, 0x34, 0x80 } };

BEGIN_INTERFACE_MAP(CBrokerIBDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CBrokerIBDlgAutoProxy, IID_IBrokerIB, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {9273E286-9F5B-4928-A272-4CA2E458C98D}
IMPLEMENT_OLECREATE2(CBrokerIBDlgAutoProxy, "BrokerIB.Application", 0x9273e286, 0x9f5b, 0x4928, 0xa2, 0x72, 0x4c, 0xa2, 0xe4, 0x58, 0xc9, 0x8d)

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlgAutoProxy message handlers


BSTR CBrokerIBDlgAutoProxy::PlaceOrder(LPCTSTR pszTicker, LPCTSTR pszAction, long nQuantity, LPCTSTR pszType, double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, const VARIANT FAR& TickSize, const VARIANT FAR& Attributes, const VARIANT FAR& ParentID, const VARIANT FAR& OCAGroup, const VARIANT FAR& OCAType, const VARIANT FAR& FAParams, const VARIANT FAR& Account ) 
{
	CString strResult;

	long lOrderID = m_pDialog->PlaceOrder( 0, pszTicker, pszAction, pszType, nQuantity, dLimitPrice, dStopPrice, pszTimeInForce, bTransmit, 0.0001 * VariantToDouble( TickSize, 100 ), VariantToString( Attributes ), atoi( VariantToString( ParentID ) ), VariantToString( OCAGroup ), atoi( VariantToString( OCAType ) ), VariantToString( FAParams ), VariantToString( Account ) );

	strResult.Format("%d", lOrderID );

	return strResult.AllocSysString();
}

BSTR CBrokerIBDlgAutoProxy::ModifyOrder(LPCTSTR pszOrderId, LPCTSTR pszTicker, LPCTSTR pszAction,  long nQuantity, LPCTSTR pszType, double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, const VARIANT FAR& TickSize, const VARIANT FAR& Attributes, const VARIANT FAR& ParentID, const VARIANT FAR& OCAGroup, const VARIANT FAR& OCAType, const VARIANT FAR& FAParams, const VARIANT FAR& Account ) 
{
	CString strResult;

	long lOrderID = m_pDialog->PlaceOrder( atoi( pszOrderId ), pszTicker, pszAction, pszType, nQuantity, dLimitPrice, dStopPrice, pszTimeInForce, bTransmit, 0.0001 * VariantToDouble( TickSize, 100 ), VariantToString( Attributes ), atoi( VariantToString( ParentID ) ), VariantToString( OCAGroup ), atoi( VariantToString( OCAType ) ), VariantToString( FAParams ), VariantToString( Account )  );

	strResult.Format("%d", lOrderID );
	
	return strResult.AllocSysString();
}

BOOL CBrokerIBDlgAutoProxy::CancelOrder(LPCTSTR pszOrderId) 
{
	return m_pDialog->CancelOrder( atoi( pszOrderId ) );
}

BSTR CBrokerIBDlgAutoProxy::GetStatus(LPCTSTR pszOrderId, const VARIANT FAR& InclFilled) 
{
	CString strResult;

	BOOL bInclFilled = VariantToDouble( InclFilled, 0 ) != 0;

	m_pDialog->GetPendingOrderStatus( atoi( pszOrderId ), bInclFilled, strResult );

	return strResult.AllocSysString();
}

BOOL CBrokerIBDlgAutoProxy::IsOrderPending(LPCTSTR pszOrderId) 
{
	return m_pDialog->IsOrderPending( atoi( pszOrderId ) );
}

long CBrokerIBDlgAutoProxy::GetPositionSize(LPCTSTR pszTicker) 
{
	return m_pDialog->GetPositionSize( pszTicker );
}

long CBrokerIBDlgAutoProxy::IsConnected() 
{
	return m_pDialog->IsConnected();
}

BSTR CBrokerIBDlgAutoProxy::GetLastError(LPCTSTR pszOrderId) 
{
	CString strResult;

	m_pDialog->GetLastError( atoi( pszOrderId ), strResult );

	return strResult.AllocSysString();
}

BSTR CBrokerIBDlgAutoProxy::GetAccountValue(LPCTSTR pszField) 
{
	CString strResult;

	m_pDialog->GetAccountValue( pszField, strResult );

	return strResult.AllocSysString();
}

VARIANT CBrokerIBDlgAutoProxy::GetPositionInfo(LPCTSTR pszTicker, LPCTSTR pszField) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);

	m_pDialog->GetPositionInfo( pszTicker, pszField, vaResult );

	return vaResult;
}

double CBrokerIBDlgAutoProxy::VariantToDouble(const VARIANT &val, double dDefault)
{
	if( val.vt == VT_EMPTY || val.vt == VT_NULL || val.vt == VT_ERROR )
	{
		return dDefault;
	}
	else
    {
        // coerce to VT_R4
        VARIANT va ;
        VariantInit( &va );
        if (SUCCEEDED(VariantChangeType( &va, (VARIANT FAR*)&val, 0, VT_R8 )))
        {
			return va.dblVal;
        }
		
	}

	return dDefault;
}

BSTR CBrokerIBDlgAutoProxy::GetPositionList() 
{
	CString strResult;

	m_pDialog->GetPositionList( strResult );

	return strResult.AllocSysString();
}

BSTR CBrokerIBDlgAutoProxy::GetPendingList(long Type, LPCTSTR pszFilter) 
{
	CString strResult;

	m_pDialog->GetPendingList( strResult, Type, pszFilter );

	return strResult.AllocSysString();
}

long CBrokerIBDlgAutoProxy::CancelAllPendingOrders( const VARIANT FAR& pszTicker ) 
{
	return 	m_pDialog->CancelAllPendingOrders( VariantToString( pszTicker ) );
}

long CBrokerIBDlgAutoProxy::CloseAllOpenPositions( const VARIANT FAR& pszTicker ) 
{
	return m_pDialog->CloseAllOpenPositions( VariantToString( pszTicker ) );
}

long CBrokerIBDlgAutoProxy::Transmit(LPCTSTR OrderId) 
{
	return 	 m_pDialog->Transmit( atoi( OrderId ) );

}

CString CBrokerIBDlgAutoProxy::VariantToString(const VARIANT &val)
{
	if( val.vt == VT_EMPTY || val.vt == VT_NULL || val.vt == VT_ERROR )
	{
		return "";
	}
	else
    {
        VARIANT va ;
        VariantInit( &va );
        if (SUCCEEDED(VariantChangeType( &va, (VARIANT FAR*)&val, 0, VT_BSTR )))
        {
			return CString( va.bstrVal );
        }
		
	}

	return "";
}

void CBrokerIBDlgAutoProxy::Sleep(long Milliseconds) 
{
	::Sleep( Milliseconds );
}

BOOL CBrokerIBDlgAutoProxy::SetAccount(LPCTSTR Account) 
{
	return m_pDialog->SetAccount( Account );
}

void CBrokerIBDlgAutoProxy::ClearList(long ListNumber) 
{
	m_pDialog->ClearList( ListNumber );
}

void CBrokerIBDlgAutoProxy::Reconnect() 
{
	m_pDialog->Disconnect();
	m_pDialog->Connect();
}

BSTR CBrokerIBDlgAutoProxy::GetExecList(long Type, LPCTSTR pszFilter) 
{
	CString strResult;

	m_pDialog->GetExecList( strResult, Type, pszFilter );

	return strResult.AllocSysString();
}

VARIANT CBrokerIBDlgAutoProxy::GetExecInfo(LPCTSTR pszOrderId, LPCTSTR pszField ) 
{
	VARIANT vaResult;
	VariantInit(&vaResult);

	m_pDialog->GetExecInfo( atoi( pszOrderId ), pszField, vaResult );

	return vaResult;

}

BOOL CBrokerIBDlgAutoProxy::SetInfoAccount(LPCTSTR Account) 
{
	return m_pDialog->SetInfoAccount( Account );

}
