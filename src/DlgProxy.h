// DlgProxy.h : header file
//

#if !defined(AFX_DLGPROXY_H__549E5090_7F86_4E8A_B800_8C7993269167__INCLUDED_)
#define AFX_DLGPROXY_H__549E5090_7F86_4E8A_B800_8C7993269167__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CBrokerIBDlg;

/////////////////////////////////////////////////////////////////////////////
// CBrokerIBDlgAutoProxy command target

class CBrokerIBDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CBrokerIBDlgAutoProxy)

	CBrokerIBDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CBrokerIBDlg* m_pDialog;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBrokerIBDlgAutoProxy)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
public:
	CString VariantToString( const VARIANT &val );
	double VariantToDouble( const VARIANT &val, double dDefault );
	virtual ~CBrokerIBDlgAutoProxy();
protected:

	// Generated message map functions
	//{{AFX_MSG(CBrokerIBDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CBrokerIBDlgAutoProxy)

	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CBrokerIBDlgAutoProxy)
	afx_msg BSTR PlaceOrder(LPCTSTR pszTicker, LPCTSTR pszAction, long nQuantity, LPCTSTR pszType, double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, const VARIANT FAR& TickSize, const VARIANT FAR& Attributes, const VARIANT FAR& ParentID, const VARIANT FAR& OCAGroup, const VARIANT FAR& OCAType, const VARIANT FAR& FAParams, const VARIANT FAR& Account);
	afx_msg BSTR ModifyOrder(LPCTSTR pszOrderId, LPCTSTR pszTicker, LPCTSTR pszAction, long nQuantity, LPCTSTR pszType,  double dLimitPrice, double dStopPrice, LPCTSTR pszTimeInForce, bool bTransmit, const VARIANT FAR& TickSize, const VARIANT FAR& Attributes, const VARIANT FAR& ParentID, const VARIANT FAR& OCAGroup, const VARIANT FAR& OCAType, const VARIANT FAR& FAParams, const VARIANT FAR& Account);
	afx_msg BOOL CancelOrder(LPCTSTR pszOrderId);
	afx_msg BSTR GetStatus(LPCTSTR pszOrderId, const VARIANT FAR& InclFilled);
	afx_msg BOOL IsOrderPending(LPCTSTR pszOrderId);
	afx_msg long GetPositionSize(LPCTSTR pszTicker);
	afx_msg long IsConnected();
	afx_msg BSTR GetLastError(LPCTSTR pszOrderId);
	afx_msg BSTR GetAccountValue(LPCTSTR pszField);
	afx_msg VARIANT GetPositionInfo(LPCTSTR pszTicker, LPCTSTR pszField);
	afx_msg BSTR GetPositionList();
	afx_msg BSTR GetPendingList(long Type, LPCTSTR pszFilter);
	afx_msg long CancelAllPendingOrders(const VARIANT FAR& pszTicker);
	afx_msg long CloseAllOpenPositions(const VARIANT FAR& pszTicker);
	afx_msg long Transmit(LPCTSTR OrderId);
	afx_msg void Sleep(long Milliseconds);
	afx_msg BOOL SetAccount(LPCTSTR Account);
	afx_msg void ClearList(long ListNumber);
	afx_msg void Reconnect();
	afx_msg BSTR GetExecList(long Type, LPCTSTR pszFilter);
	afx_msg VARIANT GetExecInfo(LPCTSTR pszOrderId, LPCTSTR pszField);
	afx_msg BOOL SetInfoAccount(LPCTSTR Account);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPROXY_H__549E5090_7F86_4E8A_B800_8C7993269167__INCLUDED_)
