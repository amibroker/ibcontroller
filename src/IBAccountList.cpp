// IBAccountList.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "IBAccountList.h"
#pragma warning( disable : 4996 )

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIBAccountList

CIBAccountList::CIBAccountList( CRect &oRect, CWnd *pParent )
{
	CreateAndInit( 13, oRect, "Name,Value,Currency", pParent ); 
	SetColumnWidth( 0, 300 );
	SetColumnWidth( 1, 150 );
	SetColumnWidth( 2, 50 );
	LoadLayout();
	//ModifyStyle( 0, LVS_SORTASCENDING, 0 );
}

CIBAccountList::~CIBAccountList()
{
}


BEGIN_MESSAGE_MAP(CIBAccountList, CBaseListCtrl)
	//{{AFX_MSG_MAP(CIBAccountList)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIBAccountList message handlers

void CIBAccountList::UpdateValue(const CString &key, const CString &val, const CString &currency, const CString &account)
{
	if( GetItemCount() == 0 ) InsertItem( 0, " - Last update time -" );


	CString oFullKey = key;

	if( ! currency.IsEmpty() && currency != "BASE" ) oFullKey = "[" + currency + "]" + key;

	LVFINDINFO oInfo;
	oInfo.flags = LVFI_STRING;
	oInfo.psz = oFullKey;

	int iItem = FindItem( &oInfo );

	if( iItem == -1 )
	{
		int iQty = GetItemCount();
		for( iItem = 0; iItem < iQty; iItem++ )
		{
			if( strcmp( oFullKey, GetItemText( iItem, 0 ) ) < 0 ) break;
		}

		iItem = InsertItem( iItem, oFullKey );
	}

	SetItemText( iItem, 1, val );
	SetItemText( iItem, 2, currency );

	if( stricmp( key, "NetLiquidation" ) == 0 ) SetItemText( 0, 1, COleDateTime::GetCurrentTime().Format( VAR_TIMEVALUEONLY ) );
}

void CIBAccountList::GetValue(LPCTSTR pszKey, CString &value)
{
	LVFINDINFO oInfo;
	oInfo.flags = LVFI_STRING;
	oInfo.psz = pszKey;

	int iItem = FindItem( &oInfo );

	if( iItem != -1 )
	{
		value = GetItemText( iItem, 1 );
		return;
	}

	oInfo.psz = CString( "[USD]" ) + pszKey;

	iItem = FindItem( &oInfo );

	if( iItem != -1 )
	{
		value = GetItemText( iItem, 1 );
	}
}
