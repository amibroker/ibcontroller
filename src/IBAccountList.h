#if !defined(AFX_IBACCOUNTLIST_H__45BD79B8_CF7C_476E_AA9F_C4E6A9E18D6D__INCLUDED_)
#define AFX_IBACCOUNTLIST_H__45BD79B8_CF7C_476E_AA9F_C4E6A9E18D6D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IBAccountList.h : header file
//
#include "BaseListCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CIBAccountList window

class CIBAccountList : public CBaseListCtrl
{
// Construction
public:
	CIBAccountList( CRect &oRect, CWnd *pParent );

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIBAccountList)
	//}}AFX_VIRTUAL

// Implementation
public:
	void GetValue(LPCTSTR pszKey, CString &value );
	void UpdateValue( const CString &key, const CString& val, const CString &currency, const CString &account );
	virtual ~CIBAccountList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIBAccountList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IBACCOUNTLIST_H__45BD79B8_CF7C_476E_AA9F_C4E6A9E18D6D__INCLUDED_)
