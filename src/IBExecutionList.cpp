// IBExecutionList.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "IBExecutionList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIBExecutionList

CIBExecutionList::CIBExecutionList(CRect &oRect, CWnd *pParent )
{
	CreateAndInit( 11, oRect, "Order ID,Action,Status,Symbol,Filled,Remaining,Avg. Price,Currency,Exchange,Account,Type", pParent ); 
}

CIBExecutionList::~CIBExecutionList()
{
}


BEGIN_MESSAGE_MAP(CIBExecutionList, CBaseListCtrl)
	//{{AFX_MSG_MAP(CIBExecutionList)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIBExecutionList message handlers

void CIBExecutionList::OrderStatus(OrderId orderId, const CString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId)
{
	CString oText;

	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	if( iItem == -1 )
	{
		oText.Format( "%d", orderId );
		iItem = InsertItem( GetItemCount(), oText );
		SetItemData( iItem, orderId );
	}

	if( GetItemText( iItem, 2 ) == "Filled" && GetItemText( iItem, 5 ) == "0" ) return;

	SetItemText( iItem, 2, status );

	oText.Format( "%d", filled	);
	SetItemText( iItem, 4, oText );

	oText.Format("%d", remaining );
	SetItemText( iItem, 5, oText );
	
	oText.Format("%g", avgFillPrice );
	SetItemText( iItem, 6, oText );

}

void CIBExecutionList::OpenOrder(OrderId orderId, const Contract &contract, const Order &order)
{
	CString oText;

	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	if( iItem == -1 )
	{
		oText.Format( "%d", orderId );
		iItem = InsertItem( GetItemCount(), oText );
		SetItemData( iItem, orderId );
	}

	if( GetItemText( iItem, 2 ) == "Filled" ) return;

	SetItemText( iItem, 1, order.action );
	SetItemText( iItem, 3, strlen( contract.localSymbol ) > 0 ? contract.localSymbol : contract.symbol );

	SetItemText( iItem, 7, contract.currency );

	SetItemText( iItem, 8, contract.exchange );

	SetItemText( iItem, 9, order.account );

	SetItemText( iItem, 10,  contract.secType );

}

int CIBExecutionList::FindOrder(long orderId)
{
	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	return iItem;
}

CString CIBExecutionList::GetOrderStatus(int iItem)
{
	return GetItemText( iItem, 2 );
}

void CIBExecutionList::OnError(long orderId, int nCode, LPCTSTR pszMessage)
{
	if( nCode == 202 )
	{
		int iItem = FindOrder( orderId );

		if( iItem >= 0 && nCode == 202 )
		{
			SetItemText( iItem, 2, "Cancelled" );
		}
	}
}

int CIBExecutionList::GetOrderID(int iItem)
{
	return atoi( GetItemText( iItem, 0 ) );
}
