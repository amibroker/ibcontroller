#if !defined(AFX_IBEXECUTIONLIST_H__681B49D3_E545_4066_BA46_D42A80AFF9E1__INCLUDED_)
#define AFX_IBEXECUTIONLIST_H__681B49D3_E545_4066_BA46_D42A80AFF9E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IBExecutionList.h : header file
//
#include "BaseListCtrl.h"
#include "EWrapper.h"
#include "Order.h"
#include "Contract.h"

/////////////////////////////////////////////////////////////////////////////
// CIBExecutionList window

class CIBExecutionList : public CBaseListCtrl
{
// Construction
public:
	CIBExecutionList(CRect &oRect, CWnd *pParent );

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIBExecutionList)
	//}}AFX_VIRTUAL

// Implementation
public:
	int GetOrderID( int iItem );
	void OnError( long orderId, int nCode, LPCTSTR pszMessage );
	int FindOrder(long orderId);
	CString GetOrderStatus(int iItem);
	void OpenOrder( OrderId orderId, const Contract &contract, const Order &order );
	void OrderStatus( OrderId orderId, const CString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId );
	virtual ~CIBExecutionList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIBExecutionList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IBEXECUTIONLIST_H__681B49D3_E545_4066_BA46_D42A80AFF9E1__INCLUDED_)
