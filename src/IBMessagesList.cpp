// IBMessagesList.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "IBMessagesList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIBMessagesList

CIBMessagesList::CIBMessagesList(CRect &oRect, CWnd *pParent)
{
	CreateAndInit( 14, oRect, "Message,Time,Repeat", pParent ); 

	SetColumnWidth( 0, oRect.Width() );
	LoadLayout();

	UpdateItemLimit();
}

CIBMessagesList::~CIBMessagesList()
{
}


BEGIN_MESSAGE_MAP(CIBMessagesList, CBaseListCtrl)
	//{{AFX_MSG_MAP(CIBMessagesList)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIBMessagesList message handlers

void CIBMessagesList::AddMessage(const CString &msg, BOOL bHilite)
{
	CTime oTime;

	CString oLastItem;

	BOOL bFound = FALSE;

	if( GetItemCount() > 0 )
	{
		if( msg.Compare( GetItemText( 0, 0 ) ) == 0 )
		{
			int iRepeat = atoi( GetItemText( 0, 2 ) );

			if( iRepeat == 0 ) iRepeat = 1;

			oLastItem.Format("%d", ++iRepeat );
	
			SetItemText( 0, 2, oLastItem );
			SetItemText( 0, 1, CTime::GetCurrentTime().Format("%H:%M:%S") );

			bFound = TRUE;
		}
	}

	if( ! bFound )
	{
		int iItem = InsertItem( 0, msg );
	
		SetItemText( iItem, 1, CTime::GetCurrentTime().Format("%H:%M:%S") );

		int iQty = GetItemCount();

		if( m_iItemLimit > 0 && iQty > m_iItemLimit )
		{
			DeleteItem( iQty - 1 );
		}
	}

	CTabCtrl *poTab = (CTabCtrl *)GetParent()->GetDlgItem( IDC_MAIN_TAB );

	if( poTab && bHilite ) poTab->HighlightItem( 4, TRUE );

}

void CIBMessagesList::UpdateItemLimit()
{
	m_iItemLimit = AfxGetApp()->GetProfileInt("Settings", "MaxErrors", 0 );

	if( m_iItemLimit > 0 && GetItemCount() > m_iItemLimit )
	{
		int iItem = GetItemCount() - 1;

		while( iItem >= m_iItemLimit )
		{
			DeleteItem( iItem-- );
		}
	}

}
