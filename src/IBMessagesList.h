#if !defined(AFX_IBMESSAGESLIST_H__0EC8DC2B_4326_4E09_9A94_28B2B76FCD18__INCLUDED_)
#define AFX_IBMESSAGESLIST_H__0EC8DC2B_4326_4E09_9A94_28B2B76FCD18__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IBMessagesList.h : header file
//
#include "BaseListCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CIBMessagesList window

class CIBMessagesList : public CBaseListCtrl
{
// Construction
public:
	CIBMessagesList(CRect &oRect, CWnd *pParent);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIBMessagesList)
	//}}AFX_VIRTUAL

// Implementation
public:
	int m_iItemLimit;
	void UpdateItemLimit( );
	void AddMessage( const CString &msg, BOOL bHilite = TRUE );
	virtual ~CIBMessagesList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIBMessagesList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IBMESSAGESLIST_H__0EC8DC2B_4326_4E09_9A94_28B2B76FCD18__INCLUDED_)
