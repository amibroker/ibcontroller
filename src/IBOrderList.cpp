// IBOrderList.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BrokerIB.h"
#include "IBOrderList.h"
#include "eclient.h"
#include "Wrapper.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CWrapper	g_oWrapper;

/////////////////////////////////////////////////////////////////////////////
// CIBOrderList

CIBOrderList::CIBOrderList(CRect &oRect, CWnd *pParent )
{
	CreateAndInit( 10, oRect, "Order ID,Action,Status,Type,Symbol,Quantity,Limit Price,Stop Price,Exchange,TimeInForce,SecType,Last Error,GoodAfterTime,GoodTillDate,Attributes,ParentID,OCAGroup,OCAType,FA Group;Profile;Method;Percentage,Account", pParent ); 
}

CIBOrderList::~CIBOrderList()
{
}


BEGIN_MESSAGE_MAP(CIBOrderList, CBaseListCtrl)
	//{{AFX_MSG_MAP(CIBOrderList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIBOrderList message handlers

void CIBOrderList::OpenOrder(OrderId orderId, const Contract &contract, const Order &order)
{
	CString oText;

	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	AddToTranslationMap( contract );

	if( iItem == -1 )
	{
		oText.Format( "%d", orderId );
		iItem = InsertItem( GetItemCount(), oText );
		SetItemData( iItem, orderId );
		SetItemText( iItem, 2, "NoStatus" );

		//CString oTrace;
		//oTrace.Format("Adding item %d", orderId );
		//OutputDebugString( oTrace );

	}
	else
	{
		if( GetItemText( iItem, 2 ) == "Error" ) return;
	}

	SetItemText( iItem, 1, order.action );
	SetItemText( iItem, 3, order.orderType );
	SetItemText( iItem, 4, strlen( contract.localSymbol ) > 0 ? contract.localSymbol : contract.symbol );

	oText.Format( "%d", order.totalQuantity	);
	SetItemText( iItem, 5, oText );

	oText.Format("%g", order.lmtPrice );
	SetItemText( iItem, 6, oText );
	
	oText.Format("%g", order.auxPrice );
	SetItemText( iItem, 7, oText );

	SetItemText( iItem, 8, contract.exchange );

	SetItemText( iItem, 9, order.tif );

	SetItemText( iItem, 10, contract.secType );

	SetItemText( iItem, 11, "" );

	SetItemText( iItem, 12, order.goodAfterTime );
	SetItemText( iItem, 13, order.goodTillDate );

	CString oAttrs;
	Order ordcopy = order;
	g_oWrapper.OrderToAttributes( ordcopy, oAttrs, TRUE );
	
	SetItemText( iItem, 14, oAttrs );

	if( order.parentId )
	{
		oText.Format("%d", order.parentId );
		SetItemText( iItem, 15, oText );
	}

	SetItemText( iItem, 16, order.ocaGroup );

	if( order.ocaType )
	{
		oText.Format("%d", order.ocaType );
		SetItemText( iItem, 17, oText );
	}

	CString oFAParams;
	g_oWrapper.OrderToFAParams( ordcopy, oFAParams, TRUE );

	SetItemText( iItem, 18, oFAParams );

	SetItemText( iItem, 19, order.account );
}

void CIBOrderList::OrderStatus(OrderId orderId, const CString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId)
{
	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	if( iItem == -1 )
	{
		TRACE("PROBLEM OrderStatus without OpenOrder!\n");
		return;
	}

	CString oItemText;

//	SetItemText( iItem, 1, order.action );
	CString oOldStatus = GetItemText( iItem, 2 );

	if( oOldStatus == "NoStatus" && status == "PendingCancel" )
	{
		SetItemText( iItem, 2, "Cancelled" );
	}
	else
	if( oOldStatus != "Error" && oOldStatus != "Cancelled" )
	{
		SetItemText( iItem, 2, status );
	}



	//CString oTrace;
	//oTrace.Format("OrderStatus %d %s", orderId, status );
	//OutputDebugString( oTrace );

//	SetItemText( iItem, 3, order.orderType );
//	SetItemText( iItem, 4, contract.symbol );

//	oText.Format( "%d", order.totalQuantity	);
//	SetItemText( iItem, 5, oText );

//	oText.Format("%g", order.lmtPrice );
//	SetItemText( iItem, 6, oText );
	
//	oText.Format("%g", order.auxPrice );
//	SetItemText( iItem, 7, oText );

//	SetItemText( iItem, 8, contract.exchange );

}

void CIBOrderList::Cleanup(BOOL bCancelOrdersWithErrors)
{
	int iQty = GetItemCount();
	for( int i = iQty - 1; i >= 0; i-- )
	{
		CString oItemText = GetItemText( i, 2 );

		if( oItemText == "NoStatus" ) DeleteItem( i );
		if( oItemText == "Filled" ) DeleteItem( i );
		if( oItemText == "Cancelled" ) DeleteItem( i );
		if( bCancelOrdersWithErrors && oItemText == "Error" ) 
		{
			long lOrderId = GetItemData( i );

			extern EClient     *g_pClient;

			int nErrorCode = atoi( GetOrderErrorMsg( i ) );

			if( nErrorCode != 135 /* can't find order id */ )
			{
				g_pClient->cancelOrder( lOrderId );
				OrderStatus( lOrderId, "Cancelled", 0, 0, 0, 0, 0, 0, 0 );
			}
			else
			{
				DeleteItem( i );
			}

		}

	}
}



void CIBOrderList::GetOrderAndContractFromItem(int iItem, Contract &contract, Order &order)
{
	order.orderId = atoi( GetItemText( iItem, 0 ) );
	order.action = GetItemText( iItem, 1 );
	// 2 - status
	order.orderType = GetItemText( iItem, 3 );
	// 
	contract.symbol = GetItemText( iItem, 4 );
	order.totalQuantity = atoi( GetItemText( iItem, 5 ) );
	order.lmtPrice = atof( GetItemText( iItem, 6 ) );
	order.auxPrice = atof( GetItemText( iItem, 7 ) );

	order.goodAfterTime = GetItemText( iItem, 12 );
	order.goodTillDate = GetItemText( iItem, 13 );
	
	contract.exchange = GetItemText( iItem, 8 );

	order.tif = GetItemText( iItem, 9 );
	contract.secType = GetItemText( iItem, 10 );

	contract.localSymbol = ( contract.secType != "STK" ) ? contract.symbol : "";

	CString oAttrs = GetItemText( iItem, 14 );
	g_oWrapper.OrderToAttributes( order, oAttrs, FALSE );

	order.parentId = atoi( GetItemText( iItem, 15 ) );

	order.ocaGroup = GetItemText( iItem, 16 );
	order.ocaType = atoi( GetItemText( iItem, 17 ) );
	
	CString oFAParams = GetItemText( iItem, 18 );
	g_oWrapper.OrderToFAParams( order, oFAParams, FALSE );

	order.account = g_oWrapper.m_oAccount;
}

CString CIBOrderList::GetOrderStatus(int iItem)
{
	return GetItemText( iItem, 2 );
}



int CIBOrderList::FindOrder(long orderId)
{
	LVFINDINFO oInfo;
	oInfo.flags = LVFI_PARAM;
	oInfo.lParam = orderId;

	int iItem = FindItem( &oInfo );

	return iItem;
}

void CIBOrderList::OnError(long orderId, int nCode, LPCTSTR pszMessage)
{
	int iItem = FindOrder( orderId );

	if( iItem >= 0 && nCode != 202 /* ignore 202 - order cancelled */ )
	{
		SetItemText( iItem, 2, "Error");
		CString oErrorText;
		oErrorText.Format("%d. %s", nCode, pszMessage );
		SetItemText( iItem, 11, oErrorText );
	}

	//CString oTrace;
	//oTrace.Format("OnError %d %d %s", orderId, nCode, pszMessage );
	//OutputDebugString( oTrace );

	if( iItem >= 0 && nCode == 202 )
	{
		SetItemText( iItem, 2, "Cancelled" );
		SetItemText( iItem, 11, pszMessage );
	}
}

CString CIBOrderList::GetOrderErrorMsg(int iItem)
{
	return GetItemText( iItem, 11 );
}



void CIBOrderList::AddToTranslationMap(const Contract &contract)
{
	if( contract.secType == "FUT" &&
		contract.localSymbol != "" &&
		contract.expiry != "" &&
		contract.symbol != "" )
	{
		// local->symbol translation map
		// key = local symbol
		// value = "symbol|expiry"

		CString oValue;
		oValue.Format( "%s|%s", contract.symbol, contract.expiry );
		g_oWrapper.m_oLocalSymbolTranslation.SetAt( contract.localSymbol, oValue );

	//	OutputDebugString( CString("Adding ") + contract.localSymbol + CString(" = " ) + oValue );
	}
}
