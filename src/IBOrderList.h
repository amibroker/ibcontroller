#if !defined(AFX_IBORDERLIST_H__17243A25_EE48_470F_A8F5_6BB5FC1C871B__INCLUDED_)
#define AFX_IBORDERLIST_H__17243A25_EE48_470F_A8F5_6BB5FC1C871B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IBOrderList.h : header file
//
#include "BaseListCtrl.h"
#include "EWrapper.h"
#include "Order.h"
#include "Contract.h"
#include "OrderData.h"


/////////////////////////////////////////////////////////////////////////////
// CIBOrderList window

class CIBOrderList : public CBaseListCtrl
{
// Construction
public:
	CIBOrderList(CRect &oRect, CWnd *pParent );

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIBOrderList)
	//}}AFX_VIRTUAL

// Implementation
public:
	void AddToTranslationMap( const Contract &contract );
	CString GetOrderErrorMsg( int iItem );
	void OnError( long orderId, int nCode, LPCTSTR pszMessage );
	int FindOrder( long orderId );
	CString GetOrderStatus( int iItem );
	void GetOrderAndContractFromItem( int iItem, Contract& contract, Order &order );
	void Cleanup( BOOL bCancelOrdersWithErrors = FALSE );
	void OrderStatus( OrderId orderId, const CString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId );
	void OpenOrder( OrderId orderId, const Contract &contract, const Order &order );
	virtual ~CIBOrderList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIBOrderList)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IBORDERLIST_H__17243A25_EE48_470F_A8F5_6BB5FC1C871B__INCLUDED_)
