// IBPortfolioList.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "IBPortfolioList.h"
#include "Wrapper.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern EClient     *g_pClient;

/////////////////////////////////////////////////////////////////////////////
// CIBPortfolioList

CIBPortfolioList::CIBPortfolioList(CRect &oRect, CWnd *pParent)
{ 	
	CreateAndInit( 12, oRect, "Symbol,Type,Exchange,Expiry,Strike,Currency,Position,Mkt. Price,Mkt. Value,Avg. Cost,Unrealized PNL,Realized PNL,Account", pParent ); 

}

CIBPortfolioList::~CIBPortfolioList()
{
}


BEGIN_MESSAGE_MAP(CIBPortfolioList, CBaseListCtrl)
	//{{AFX_MSG_MAP(CIBPortfolioList)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIBPortfolioList message handlers

void CIBPortfolioList::UpdatePortfolio(const Contract &contract, int position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, const CString &accountName)
{
	CString oText;


	int iItem = FindContract( contract );

	if( position == 0 ) 
	{
		if( iItem != -1 ) DeleteItem( iItem );
		return;
	}

	if( iItem == -1 )
	{
		iItem = InsertItem( GetItemCount(), contract.localSymbol );
	}

	SetItemText( iItem, 1, contract.secType );
	if( !contract.primaryExchange.IsEmpty() )
		SetItemText( iItem, 2, contract.primaryExchange );
	else
	if( !contract.exchange.IsEmpty() )
		SetItemText( iItem, 2, contract.exchange );
	else
	if( contract.secType != "STK" || contract.currency != "USD" )
	{
		SetItemText( iItem, 2, "" );
		g_pClient->reqContractDetails( contract );
	}

	TRACE("UpdatePortfolio %s, %s, %s, %s\n", contract.localSymbol, contract.exchange, contract.primaryExchange, contract.secType );


	SetItemText( iItem, 3, contract.expiry );

	oText.Format("%g", contract.strike );
	SetItemText( iItem, 4, oText );

	SetItemText( iItem, 5, contract.currency );

	oText.Format("%d", position );
	SetItemText( iItem, 6, oText );

	oText.Format("%g", marketPrice );
	SetItemText( iItem, 7, oText );

	oText.Format("%g", marketValue );
	SetItemText( iItem, 8, oText );

	oText.Format("%g", averageCost );
	SetItemText( iItem, 9, oText );

	oText.Format("%g", unrealizedPNL );
	SetItemText( iItem, 10, oText );

	oText.Format("%g", realizedPNL );
	SetItemText( iItem, 11, oText );

	SetItemText( iItem, 12, accountName );

}

int CIBPortfolioList::FindContract(const Contract &contract)
{
	LVFINDINFO oInfo;
	oInfo.flags = LVFI_STRING;
	oInfo.psz = strlen( contract.localSymbol ) > 0 ? contract.localSymbol : contract.symbol;

	int iItem = -1;
	
	while( ( iItem = FindItem( &oInfo, iItem ) ) >= 0 )
	{
		if( GetItemText( iItem, 1 ) == contract.secType )
		{
			CString oExchange = GetItemText( iItem, 2 );
			if( oExchange.IsEmpty() || 
				contract.primaryExchange.IsEmpty() ||
			    oExchange == contract.primaryExchange )
			{
				break;
			}
		}
	}

	return iItem;

}

int CIBPortfolioList::GetPositionSize(int iItem)
{
	return atoi( GetItemText( iItem, 6 ) );
}

CString CIBPortfolioList::GetPositionSymbol(int iItem)
{
	extern CWrapper	g_oWrapper;

	Contract ctr;

	ctr.exchange = ctr.primaryExchange = GetItemText( iItem, 2 );
    ctr.secType = GetItemText( iItem, 1 );

	if( ctr.secType != "STK" )
		ctr.localSymbol = GetItemText( iItem, 0 );
	else
		ctr.symbol = GetItemText( iItem, 0 );

	// v1.40 fix: handle non-US currency
	ctr.currency = GetItemText( iItem, 5 );

	CString oSymbol = g_oWrapper.ContractToTicker( ctr );

	return oSymbol;
}

void CIBPortfolioList::OnContractDetails(const ContractDetails& cd)
{
	int iItem = FindContract( cd.summary );

	if( iItem != -1 )
	{
		SetItemText( iItem, 2, cd.summary.exchange );
	}

}

CString CIBPortfolioList::GetPositionAccount(int iItem)
{
	return GetItemText( iItem, 12 );
}
