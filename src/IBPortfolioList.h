#if !defined(AFX_IBPORTFOLIOLIST_H__3B4C0EED_8424_492F_81F7_B5FF25C07BE1__INCLUDED_)
#define AFX_IBPORTFOLIOLIST_H__3B4C0EED_8424_492F_81F7_B5FF25C07BE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IBPortfolioList.h : header file
//
#include "BaseListCtrl.h"
#include "EWrapper.h"
#include "eclient.h"
#include "Order.h"
#include "Contract.h"

/////////////////////////////////////////////////////////////////////////////
// CIBPortfolioList window

class CIBPortfolioList : public CBaseListCtrl
{
// Construction
public:
	CIBPortfolioList(CRect &oRect, CWnd *pParent);

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIBPortfolioList)
	//}}AFX_VIRTUAL

// Implementation
public:
	CString GetPositionAccount( int iItem );
	void OnContractDetails( const ContractDetails& cd );
	CString GetPositionSymbol( int iItem );
	int GetPositionSize( int iItem );
	int FindContract(const Contract &contract );
	void UpdatePortfolio( const Contract& contract, int position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, const CString & accountName );
	virtual ~CIBPortfolioList();

	// Generated message map functions
protected:
	//{{AFX_MSG(CIBPortfolioList)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IBPORTFOLIOLIST_H__3B4C0EED_8424_492F_81F7_B5FF25C07BE1__INCLUDED_)
