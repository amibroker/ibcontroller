// IgnoreListDlg.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "brokerib.h"
#include "IgnoreListDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIgnoreListDlg dialog


CIgnoreListDlg::CIgnoreListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIgnoreListDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIgnoreListDlg)
	m_oIgnoreList = _T("");
	//}}AFX_DATA_INIT
	m_pNumArray = NULL;
}


void CIgnoreListDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIgnoreListDlg)
	DDX_Text(pDX, IDC_IGNORE_EDIT, m_oIgnoreList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIgnoreListDlg, CDialog)
	//{{AFX_MSG_MAP(CIgnoreListDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIgnoreListDlg message handlers

BOOL CIgnoreListDlg::OnInitDialog() 
{
	for( int i = 0; i < m_pNumArray->GetSize(); i++ )
	{
		CString oTemp;
		oTemp.Format("%s%d", i == 0 ? "" : ", ", m_pNumArray->GetAt( i ) );

		m_oIgnoreList += oTemp;
	}

	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CIgnoreListDlg::OnOK() 
{
	if( UpdateData( TRUE ) )
	{
	   AfxGetApp()->WriteProfileString("Settings", "IgnoreList", m_oIgnoreList );

		CString oSubStr;

		m_pNumArray->RemoveAll();

		for( int i = 0; AfxExtractSubString( oSubStr, m_oIgnoreList, i, ',' ); i++ )
		{
			int iNum = atoi( oSubStr );
			if( iNum > 0 ) m_pNumArray->Add( iNum );
		}
	}
	
	CDialog::OnOK();
}
