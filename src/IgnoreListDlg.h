#if !defined(AFX_IGNORELISTDLG_H__814A7BC1_687A_471C_9904_1045486D2B7B__INCLUDED_)
#define AFX_IGNORELISTDLG_H__814A7BC1_687A_471C_9904_1045486D2B7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IgnoreListDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIgnoreListDlg dialog

class CIgnoreListDlg : public CDialog
{
// Construction
public:
	CIgnoreListDlg(CWnd* pParent = NULL);   // standard constructor

	CUIntArray *m_pNumArray;

// Dialog Data
	//{{AFX_DATA(CIgnoreListDlg)
	enum { IDD = IDD_IGNORE_DIALOG };
	CString	m_oIgnoreList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIgnoreListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIgnoreListDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IGNORELISTDLG_H__814A7BC1_687A_471C_9904_1045486D2B7B__INCLUDED_)
