// OrderData.h: interface for the COrderData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ORDERDATA_H__0D15DC4E_EBA7_42EE_BB79_A62F17CCE4C6__INCLUDED_)
#define AFX_ORDERDATA_H__0D15DC4E_EBA7_42EE_BB79_A62F17CCE4C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Order.h"
#include "Contract.h"


class COrderData  
{
public:
	COrderData();
	virtual ~COrderData();

	Order		m_order;
	Contract	m_contract;
};

#endif // !defined(AFX_ORDERDATA_H__0D15DC4E_EBA7_42EE_BB79_A62F17CCE4C6__INCLUDED_)
