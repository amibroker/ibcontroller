// SymbolData.cpp: implementation of the CSymbolData class.
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "brokerib.h"
#include "SymbolData.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


int CFIFOSymbolMap::m_nAccessCounter = 0;

// returns TRUE if symbol was already in the list
BOOL CFIFOSymbolMap::Find(LPCTSTR pszSymbol, CSymbolData& oData )
{
	BOOL bRet = Lookup( pszSymbol, oData );

	if( bRet )
	{
		oData.m_iLastAccess = ++m_nAccessCounter;
	}

	return bRet;
}	


CString CFIFOSymbolMap::RemoveLeastRecentlyUsed( void )
{
	int iLRAccess	= 0x7FFFFFFFL;
	CString oLRSymbol	= "";

	POSITION hPos = GetStartPosition();

	while( hPos )
	{
		CString oKey;
		CSymbolData oData;
		GetNextAssoc( hPos, oKey, oData );
		
		if( oData.m_iLastAccess < iLRAccess )
		{
			iLRAccess = oData.m_iLastAccess;
			oLRSymbol = oKey;
		}
	}

	RemoveKey( oLRSymbol );

	return oLRSymbol;
}
