// SymbolData.h: interface for the CSymbolData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SYMBOLDATA_H__1FCA584E_AC29_426A_B702_E2E4EE65FECC__INCLUDED_)
#define AFX_SYMBOLDATA_H__1FCA584E_AC29_426A_B702_E2E4EE65FECC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSymbolData  
{
public:
	CSymbolData() 
	{ 
		m_fHigh =
		m_fLow 	=
		m_fLast	=
		m_fAsk 	=
		m_fBid  = 0;
		m_iLastSize =
		m_iVolume   =
		m_iBidSize  =
		m_iAskSize  =
		m_iLastAccess = 0; 
	};

	float	m_fHigh;
	float	m_fLow;
	float	m_fLast;
	float	m_fAsk;
	float	m_fBid;
	int		m_iLastSize;
	int		m_iVolume;
	int		m_iBidSize;
	int		m_iAskSize;
	int		m_iLastAccess;
};

typedef CMap< CString, LPCTSTR, CSymbolData, CSymbolData > CSymbolMap;

class CFIFOSymbolMap : public CSymbolMap
{
public:
	CString RemoveLeastRecentlyUsed( void);
	BOOL Find( LPCTSTR pszSymbol, CSymbolData &oData );


	static int m_nAccessCounter;
};

#endif // !defined(AFX_SYMBOLDATA_H__1FCA584E_AC29_426A_B702_E2E4EE65FECC__INCLUDED_)
