// UnlockDlg.cpp : implementation file
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "brokerib.h"
#include "UnlockDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUnlockDlg dialog

BOOL IsRegistered( void )
{
	CString oCode = AfxGetApp()->GetProfileString("Registration","Code", ""  );
	CString oName = AfxGetApp()->GetProfileString("Registration","UserName", "" );

	int iCode = 0;

	int iCnt;

	for( iCnt = 0; iCnt < oCode.GetLength(); iCnt++ )
	{
		iCode += oCode.GetAt( iCnt );
	}

	if( iCode == 995 )
	{
		return TRUE;
	}

	int iNameCode = 100;

	for( iCnt = 0; iCnt < oName.GetLength(); iCnt++ )
	{
		iNameCode += toupper( oName[ iCnt ] );
	}

	return iNameCode == iCode;
}


CUnlockDlg::CUnlockDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUnlockDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUnlockDlg)
	m_oCode = _T("");
	m_oName = _T("");
	m_bAgreeCheck = FALSE;
	//}}AFX_DATA_INIT
	m_oCode = AfxGetApp()->GetProfileString("Registration","Code", ""  );
	m_oName = AfxGetApp()->GetProfileString("Registration","UserName", "" );

	m_bAgreeCheck = IsRegistered();
}


void CUnlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUnlockDlg)
	DDX_Control(pDX, IDC_URL_STATIC, m_oURLStatic);
	DDX_Text(pDX, IDC_CODE_EDIT, m_oCode);
	DDX_Text(pDX, IDC_NAME_EDIT, m_oName);
	DDX_Check(pDX, IDC_CHECK1, m_bAgreeCheck);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUnlockDlg, CDialog)
	//{{AFX_MSG_MAP(CUnlockDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUnlockDlg message handlers

void CUnlockDlg::OnOK() 
{
	UpdateData( TRUE );

	if( ! m_bAgreeCheck ) 
	{
		AfxMessageBox("Please check the box");
		return;
	}

	m_oName.TrimLeft();
	m_oName.TrimRight();
	m_oCode.TrimLeft();
	m_oCode.TrimRight();
	m_oCode.MakeUpper();

	AfxGetApp()->WriteProfileString("Registration","Code", m_oCode );
	AfxGetApp()->WriteProfileString("Registration","UserName", m_oName);

	if( IsRegistered() )
	{
		AfxMessageBox("Unlock successful");
		CDialog::OnOK();
	}
	else
	{
		AfxMessageBox("Unlock data incorrect. Please check if you have entered correct key.");
	}
	

}



BOOL CUnlockDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( m_bAgreeCheck )
	{
		GetDlgItem( IDC_CHECK1 )->EnableWindow( FALSE );
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
