#if !defined(AFX_UNLOCKDLG_H__7ED11E82_7AD5_4637_B6A5_2482275DB074__INCLUDED_)
#define AFX_UNLOCKDLG_H__7ED11E82_7AD5_4637_B6A5_2482275DB074__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UnlockDlg.h : header file
//
#include "StatLink.h"

/////////////////////////////////////////////////////////////////////////////
// CUnlockDlg dialog
extern BOOL IsRegistered( void );

class CUnlockDlg : public CDialog
{
// Construction
public:
	CUnlockDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CUnlockDlg)
	enum { IDD = IDD_UNLOCK_DIALOG };
	CStaticLink	m_oURLStatic;
	CString	m_oCode;
	CString	m_oName;
	BOOL	m_bAgreeCheck;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUnlockDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CUnlockDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNLOCKDLG_H__7ED11E82_7AD5_4637_B6A5_2482275DB074__INCLUDED_)
