// Wrapper.cpp: implementation of the CWrapper class.
//
//////////////////////////////////////////////////////////////////
// Copyright (C)2004-2013 AmiBroker.com/Tomasz Janeczko. All rights reserved.
//
// IBController source codes are licensed under
// Creative Commons Attribution-ShareAlike 3.0 Unported  (CC BY-SA 3.0) License
// http://creativecommons.org/licenses/by-sa/3.0/
//////////////////////////////////////////////////////////////////////
//
// DISCLAIMER
//
// AMIBROKER.COM MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE CODE FOR ANY PURPOSE. 
// IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND. 
// AMIBROKER.COM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOURCE CODE, 
// INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// IN NO EVENT SHALL AMIBROKER.COM BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR 
// CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
// WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
// ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOURCE CODE.
///////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BrokerIB.h"
#include "Wrapper.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWrapper::CWrapper()
{
	m_iConnected = 0;
	m_iPeriodicity = 60;
	m_poAccountList = NULL;
	m_poExecutionList =	NULL;
	m_poMessagesList = NULL;
	m_poOrderList =	NULL;
	m_poPortfolioList = NULL;
	m_nLastOrderID = 1000;

}

CWrapper::~CWrapper()
{

}

void CWrapper::tickPrice( TickerId tickerId, TickType field, double price, int canAutoExecute)
{ 
#ifndef NO_QUOTE_DATA
	LPCTSTR pszTicker = (LPCTSTR) tickerId;
	CSymbolData *poSymData = m_oSymbols.FindSymbol( pszTicker );

	if( poSymData )
	{
		time_t curtime = time(NULL);

		switch( field )
		{
			case BID:		
				poSymData->m_oRecentInfo.fBid = (float) price;
				break;
			case ASK:		
				poSymData->m_oRecentInfo.fAsk = (float) price;
				break;	
			case LAST:		
				TRACE("LAST PRICE %s, %f\n", pszTicker, price );
				poSymData->m_oRecentInfo.fLast = (float) price;
				if( poSymData->m_oRecentInfo.fPrev > 0 ) poSymData->m_oRecentInfo.fChange = poSymData->m_oRecentInfo.fLast - poSymData->m_oRecentInfo.fPrev;
				break;			
			
			case HIGH:		
				poSymData->m_oRecentInfo.fHigh = (float) price;
				break;

			case LOW:		
				poSymData->m_oRecentInfo.fLow = (float) price;
				break;

			//case VOLUME:	
			case CLOSE:		
				TRACE("CLOSE PRICE %s, %f\n", pszTicker, price );
		   		poSymData->m_oRecentInfo.fPrev = (float) price;

				if( poSymData->m_oRecentInfo.fLast == 0 )
				{
					poSymData->m_oRecentInfo.fLast = (float) price;
				}

				poSymData->m_oRecentInfo.fChange = poSymData->m_oRecentInfo.fLast - poSymData->m_oRecentInfo.fPrev;

				break;

			default:
				break;
		}

		struct tm *curtm = localtime( &curtime );

		poSymData->m_oRecentInfo.nBitmap = RI_LAST | RI_HIGHLOW | RI_TRADEVOL  | 
		RI_TOTALVOL | RI_PREVCHANGE | RI_BID | RI_ASK | RI_DATEUPDATE | RI_DATECHANGE;	

		poSymData->m_oRecentInfo.nStatus = RI_STATUS_UPDATE | RI_STATUS_BIDASK | RI_STATUS_TRADE | RI_STATUS_BARSREADY;


		poSymData->m_oRecentInfo.nDateChange = poSymData->m_oRecentInfo.nDateUpdate = curtm->tm_year * 10000 + ( curtm->tm_mon + 1 ) * 100 + curtm->tm_mday;
		poSymData->m_oRecentInfo.nTimeChange = poSymData->m_oRecentInfo.nTimeUpdate = curtm->tm_hour * 10000 + ( curtm->tm_min ) * 100 + curtm->tm_sec;


	}
#endif

}

void CWrapper::tickSize( TickerId tickerId, TickType field, int size) 
{

#ifndef NO_QUOTE_DATA

	LPCTSTR pszTicker = (LPCTSTR) tickerId;
	CSymbolData *poSymData = m_oSymbols.FindSymbol( pszTicker );

	if( poSymData )
	{
		switch( field )
		{
			case BID_SIZE:
				poSymData->m_oRecentInfo.iBidSize = size;
				break;
			case ASK_SIZE:
				poSymData->m_oRecentInfo.iAskSize = size;
				break;	
			case LAST_SIZE:	
				poSymData->m_oRecentInfo.iTradeVol = size;
				TRACE("LAST SIZE %s, %d\n", pszTicker, size );
				poSymData->AddTick( time( NULL ), poSymData->m_iLastTick+1, poSymData->m_oRecentInfo.fLast, size );
				//poSymData->AddTick( time( NULL), poSymData->m_iLastTick+1, (float) price, poSymData->m_oRecentInfo.iTradeVol );
				break;			
			
			case VOLUME:	
		   		poSymData->m_oRecentInfo.iTotalVol = size;
				break;

			default:
				break;
		}

		m_iConnected = CONN_OK;

		::SendMessage( m_hAmiWnd, WM_USER_STREAMING_UPDATE, (WPARAM)(LPCTSTR)poSymData->m_oSymbol, (LPARAM)&poSymData->m_oRecentInfo );

	}

#endif
}




void CWrapper::ConnectLists(CBaseListCtrl **apoListCtrls)
{
	CString oIgnoreList = AfxGetApp()->GetProfileString("Settings", "IgnoreList", "2109" );

	CString oSubStr;

	m_oIgnoreArray.RemoveAll();

	for( int i = 0; AfxExtractSubString( oSubStr, oIgnoreList, i, ',' ); i++ )
	{
		int iNum = atoi( oSubStr );
		if( iNum > 0 ) m_oIgnoreArray.Add( iNum );
	}

	m_poAccountList =	(CIBAccountList *)	apoListCtrls[ LIST_ACCOUNT ];
	m_poExecutionList =	(CIBExecutionList *)apoListCtrls[ LIST_EXECUTION ];
	m_poMessagesList =	(CIBMessagesList *)	apoListCtrls[ LIST_MESSAGE ];
	m_poOrderList =		(CIBOrderList *)	apoListCtrls[ LIST_ORDER ];
	m_poPortfolioList = (CIBPortfolioList *)apoListCtrls[ LIST_PORTFOLIO ];
}

void CWrapper::updateAccountValue(const CString& key, const CString& val,	const CString& currency, const CString& accountName) 
{ 
//	TRACE("%s\n", key );

	if( m_poAccountList ) m_poAccountList->UpdateValue( key, val, currency, accountName );
}

void CWrapper::winError( const CString &str, int lastError) 
{
	TRACE( "WinError %s\n", str ); 

	if( m_poMessagesList ) m_poMessagesList->AddMessage( str );
}

void CWrapper::error(const int id, const int errorCode, const CString errorString) 
{ 
	m_oLastError.Format( "Message from TWS %d, %d, %s\n", id, errorCode, errorString ); 
	TRACE( m_oLastError ); 
	if( m_iConnected == CONN_OK ) m_iConnected = CONN_MINOR_PROBLEM; 
	CString oErrorMsg;

	if( errorCode == 504 || errorCode == 502 ) m_iConnected = CONN_DISCONNECTED;

	if( errorCode == 505 ) /* Fatal Error: Unknown message id. */
	{
		return; // do nothing in case of error 505
	}

	if( errorCode == 2107 || errorCode == 2104) 
	{
		//ID=-1.Error 2107.HMDS data farm connection is inactive buit should be 
		//available upon demand.: ushmds2a
		// Error 2104 - market data connection OK
		return; // do nothing
	}

	if( errorCode == 321 && errorString.Find("ReqAcctData") != -1 &&
		m_oAccountsList.Find( m_oAccount ) != -1 )
	{
		// do nothing
		return;
	}

	/*
	if( errorCode == 321 && errorString.Find("ReqManagedAccts") != -1 )
	{
		// do nothing if 
		// "ReqManagedAccts cause only FA or STL customers can call this

		return;
	}
	*/

	if( errorCode == 2100 || errorCode == 1300 || errorCode == 1100 || errorCode == 1101 || errorCode == 1102 )
	{
		// new account data requested, API client has been unsubscribed from updates
		m_poPortfolioList->DeleteAllItems();
		m_poOrderList->DeleteAllItems();

		extern EClient     *g_pClient;

		g_pClient->reqAccountUpdates( TRUE, m_oInfoAccount );
  		g_pClient->reqOpenOrders();

	}


	for( int ign = 0; ign < m_oIgnoreArray.GetSize(); ign++ )
	{
		if( errorCode == m_oIgnoreArray[ ign ] ) 
		{
			oErrorMsg.Format("ID=%d. Error (ignored) %d. %s", id, errorCode, errorString );
			// ignore it
			if( m_poMessagesList ) m_poMessagesList->AddMessage( oErrorMsg );

			return;
		}
	}

	oErrorMsg.Format("ID=%d. Error %d. %s", id, errorCode, errorString );
	if( m_poMessagesList ) m_poMessagesList->AddMessage( oErrorMsg );

	if( m_poOrderList ) m_poOrderList->OnError( id, errorCode, errorString );
	if( m_poExecutionList ) m_poExecutionList->OnError( id, errorCode, errorString );
}

void CWrapper::orderStatus( OrderId orderId, const CString &status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, const CString& whyHeld) 
{ 
	if( orderId >= m_nLastOrderID ) m_nLastOrderID = orderId + 1;

	TRACE("OrderStatus %d, %s, filled=%d, rem=%d, avgfill=%g, lastfill=%g\n", orderId, status, filled, remaining, avgFillPrice, lastFillPrice  ); 

	if( m_poOrderList ) m_poOrderList->OrderStatus( orderId, status, filled, remaining, avgFillPrice, permId, parentId, lastFillPrice, clientId );
	if( m_poExecutionList ) m_poExecutionList->OrderStatus( orderId, status, filled, remaining, avgFillPrice, permId, parentId, lastFillPrice, clientId );
}

void CWrapper::openOrder( OrderId orderId, const Contract &contract, const Order &order, const OrderState &state ) 
{ 
	TRACE("OpenOrder %d\n", orderId );

	if( orderId >= m_nLastOrderID ) m_nLastOrderID = orderId + 1;

	if( m_poOrderList ) m_poOrderList->OpenOrder( orderId, contract, order );
	if( m_poExecutionList ) m_poExecutionList->OpenOrder( orderId, contract, order );

}

void CWrapper::updatePortfolio( const Contract& contract, int position,
		double marketPrice, double marketValue, double averageCost,
		double unrealizedPNL, double realizedPNL, const CString& accountName )
{

	if( m_poPortfolioList ) m_poPortfolioList->UpdatePortfolio(	contract, position,
		marketPrice, marketValue,  averageCost,
		 unrealizedPNL,  realizedPNL, accountName );

}

void CWrapper::contractDetails( const ContractDetails& contractDetails) 
{
	if( m_poPortfolioList ) m_poPortfolioList->OnContractDetails( contractDetails );
}


void CWrapper::TickerToContract(LPCTSTR pszTicker, Contract &contract)
{
	CString oTicker;
	CString oType;
	CString oExchange;
	CString oCurrency;

	if( AfxExtractSubString( oType, pszTicker, 3, '-') && 
		(	oType.CompareNoCase( "IND" ) == 0 || 
			oType.CompareNoCase( "I" ) == 0 ||
			AfxExtractSubString( oCurrency, pszTicker, 4, '-') 
		) 
	  )
	{
		CString oTicker2;
		AfxExtractSubString( oTicker, pszTicker, 0, '-' );
		AfxExtractSubString( oTicker2, pszTicker, 1, '-' );

		oTicker += "-" + oTicker2;

		AfxExtractSubString( oExchange, pszTicker, 2, '-');
		AfxExtractSubString( oCurrency, pszTicker, 4, '-');
	}
	else
	{
		AfxExtractSubString( oTicker, pszTicker, 0, '-' );
		AfxExtractSubString( oExchange, pszTicker, 1, '-');
		AfxExtractSubString( oType, pszTicker, 2, '-');
		AfxExtractSubString( oCurrency, pszTicker, 3, '-');
	}

	oType.MakeUpper();
	oExchange.MakeUpper();
	oCurrency.MakeUpper();

	if( oType.IsEmpty() ) oType = "STK";
	if( oExchange.IsEmpty() ) oExchange = "SMART";
	if( oCurrency.IsEmpty() && oType != "CASH"  ) oCurrency = "USD";


	if( oType == "OP" ) oType = "OPT";
	if( oType == "O" ) oType = "OPT";
	if( oType == "F" ) oType = "FUT";
	if( oType == "S" ) oType = "STK";
	if( oType == "I" ) oType = "IND";
	if( oType == "C" ) oType = "CASH";
	if( oType == "P" ) oType = "FOP";

	static std::vector<ComboLeg*> comboLegs;

	contract.symbol		= ( oType != "STK" ) ? "" : oTicker;
	contract.secType	= oType;//s_dlg.m_type;
	contract.expiry		= "";// s_dlg.m_expiry;
	contract.strike		= 0.0f;//s_dlg.m_strike;
	contract.right		= "";//s_dlg.m_right;
	contract.multiplier = "";
	contract.exchange	= oExchange;//s_dlg.m_exchange;
	contract.primaryExchange = ""; //oExchange == "SMART" ? "" : oExchange;
	contract.currency	= oCurrency;//s_dlg.m_currency;
	contract.localSymbol= ( oType != "STK" ) ? oTicker : "";
	contract.comboLegs = NULL;//&comboLegs;

	if( oType == "FUT" )
	{
		// first lookup translation map as it is more reliable overall
		CString oSymExpiry;

		if( m_oLocalSymbolTranslation.Lookup( contract.localSymbol, oSymExpiry ) )
		{
			AfxExtractSubString( contract.symbol, oSymExpiry, 0, '|' );
			AfxExtractSubString( contract.expiry, oSymExpiry, 1, '|' );
		}
		else
		{
			/*
			// THIS MUST BE CHANGED TO SOMETHING THAT IS MORE RELIABLE
			// some exchanges ECBOT and DTB and possibly others ??? may use MMM YY expiration 
			// therefore relying on specific exchange is not good
			if(	oExchange == "ECBOT" )
			{
				contract.symbol = oTicker.Left( 4 );
				contract.symbol.TrimRight();
			}
			else
			{
				int iLen = GetExpiryFromLocalSymbol( contract.localSymbol, contract.expiry );

				// this unfortunatelly creates problems with symbols like
				// 6EU0 because symbol is not 6E but EUR
				if( iLen )
				{
					contract.symbol = oTicker.Left( oTicker.GetLength() - 2 );
				}
			}
			*/
		}
	}
}

CString CWrapper::ContractToTicker(Contract &contract)
{
	CString oSymbol;

	oSymbol = contract.symbol;

	if( ! contract.secType.IsEmpty() && contract.secType != "STK" && ! contract.localSymbol.IsEmpty() )
	{
		oSymbol = contract.localSymbol;
	}

	if( !contract.exchange.IsEmpty() && 
		!contract.secType.IsEmpty() )
	{
		oSymbol += "-" + contract.exchange + "-" + contract.secType;

	}
	else
	if( contract.secType != "STK" )
	{
		oSymbol += "--" + contract.secType;
	}

	if( ! contract.currency.IsEmpty() && contract.currency != "USD" )
		oSymbol += "-" + contract.currency;

	return oSymbol;

}

void CWrapper::OrderToAttributes(Order &ord, CString &attrs, BOOL bFromOrder)
{
	if( bFromOrder )
	{
		if( ord.outsideRth )
			attrs += "outsideRth ";

		if( ord.eTradeOnly )
			attrs += "eTradeOnly ";

		if( ord.firmQuoteOnly )
			attrs += "firmQuoteOnly ";
	}
	else
	{
		ord.outsideRth     = false;
		ord.allOrNone		= false;
		ord.eTradeOnly	= false;
		ord.firmQuoteOnly = false;

		if( ! attrs.IsEmpty() )
		{
			attrs.MakeLower();

			if( attrs.Find( "outsiderth" ) >= 0 ) ord.outsideRth = true;
			if( attrs.Find( "allornone" ) >= 0 ) ord.allOrNone = true;
			if( attrs.Find( "etradeonly" ) >= 0 ) ord.eTradeOnly = true;
			if( attrs.Find( "firmquoteonly" ) >= 0 ) ord.firmQuoteOnly = true;
		}
	}

}

void CWrapper::OrderToFAParams(Order &ord, CString &params, BOOL bFrom)
{
	if( bFrom )
	{
		if( ! 
			( ord.faGroup.IsEmpty() &&
			  ord.faProfile.IsEmpty() &&
			  ord.faMethod.IsEmpty() ) )
		{
			params.Format("%s;%s;%s;%s",
								ord.faGroup,
								ord.faProfile,
								ord.faMethod,
								ord.faPercentage );
		}
	}
	else
	{
		AfxExtractSubString( ord.faGroup, params, 0, ';' );
		AfxExtractSubString( ord.faProfile, params, 1, ';' );
		AfxExtractSubString( ord.faMethod, params, 2, ';' );
		AfxExtractSubString( ord.faPercentage, params, 3, ';' );
	}
}

void CWrapper::managedAccounts( const CString& accountsList) 
{
	m_oAccountsList = accountsList;
	AfxExtractSubString( m_oAccount, m_oAccountsList, 0, ',' );
	
	if( ! m_oAccount.IsEmpty() ) 
	{
		m_oInfoAccount = m_oAccount + "A";
		m_oAccountsList	= m_oInfoAccount + "," + m_oAccountsList;

		extern EClient     *g_pClient;
	
		g_pClient->reqAccountUpdates( TRUE, m_oInfoAccount );
	}

	if( m_poMessagesList ) m_poMessagesList->AddMessage( "Using FA account " + m_oAccount, FALSE );
	if( m_poMessagesList ) m_poMessagesList->AddMessage( "Available accounts: " + m_oAccountsList, FALSE );

}

BOOL CWrapper::IsFAAccount(const CString& account )
{
   return ! account.IsEmpty() &&	( account[ 0 ] == 'F' || account.Left( 2 ) == "DF" );
}

int CWrapper::GetExpiryFromLocalSymbol(CString oLocal, CString &oExpiry)
{
	CString oR2 = oLocal.Right( 2 );

	if( oR2.GetLength() == 2 )
	{
		int iOneDigitYear = ( oR2.GetAt( 1 ) - '0' );

		if( iOneDigitYear >= 0 && iOneDigitYear <= 9 )
		{
			char cThisMonth = oR2.GetAt( 0 );

			char aMonthCodes[] = "FGHJKMNQUVXZ";

			for( int i = 0; i < sizeof( aMonthCodes ); i++ )
			{
				if( aMonthCodes[ i ] == cThisMonth )
				{
					// found
					oExpiry.Format("%04d%02d", 2010 + iOneDigitYear, i + 1 );
					
					return 2; // two-character month code found
				}
			}
		}
	}

	return 0; // not found	
}
