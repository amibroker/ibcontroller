// Wrapper.h: interface for the CWrapper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WRAPPER_H__D6A4BB11_D310_4FD9_95BD_061F33E536DE__INCLUDED_)
#define AFX_WRAPPER_H__D6A4BB11_D310_4FD9_95BD_061F33E536DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "EWrapper.h"

#define NO_QUOTE_DATA 1

#ifndef NO_QUOTE_DATA
#include "SymbolData.h"	// Added by ClassView
#endif

#include "IBAccountList.h"
#include "IBExecutionList.h"
#include "IBMessagesList.h"
#include "IBOrderList.h"
#include "IBPortfolioList.h"
#include "SymbolData.h"

enum { CONN_DISCONNECTED, CONN_INPROGRESS, CONN_OK, CONN_MINOR_PROBLEM };

class CWrapper : public EWrapper  
{
public:
	CWrapper();
	virtual ~CWrapper();

	virtual void tickPrice( TickerId tickerId, TickType field, double price, int canAutoExecute);
	virtual void tickSize( TickerId tickerId, TickType field, int size);
   virtual void tickOptionComputation( TickerId tickerId, TickType tickType, double impliedVol, double delta,
	   double modelPrice, double pvDividend) {};
   virtual void tickGeneric(TickerId tickerId, TickType tickType, double value) {};
   virtual void tickString(TickerId tickerId, TickType tickType, const CString& value) {};
   virtual void tickEFP(TickerId tickerId, TickType tickType, double basisPoints, const CString& formattedBasisPoints,
	   double totalDividends, int holdDays, const CString& futureExpiry, double dividendImpact, double dividendsToExpiry) {};
   virtual void orderStatus( OrderId orderId, const CString &status, int filled,
	   int remaining, double avgFillPrice, int permId, int parentId,
	   double lastFillPrice, int clientId, const CString& whyHeld);
   virtual void openOrder( OrderId orderId, const Contract&, const Order&, const OrderState&);
	virtual void winError( const CString &str, int lastError );
	virtual void connectionClosed() 
	{ 
		TRACE( "Connection CLOSED by TWS" ); 
		m_oLastError = "ID=-1. Error 0. Connection to TWS has closed"; 
		if( m_poMessagesList ) m_poMessagesList->AddMessage( m_oLastError );
		m_iConnected = CONN_DISCONNECTED; 
	};
	virtual void updateAccountValue(const CString& key, const CString& val,	const CString& currency, const CString& accountName);
	virtual void updatePortfolio( const Contract& contract, int position,
		double marketPrice, double marketValue, double averageCost,
		double unrealizedPNL, double realizedPNL, const CString& accountName );
	virtual void updateAccountTime(const CString& timeStamp) {};
	virtual void nextValidId( OrderId orderId) 
	{	
		TRACE("NEXT VALID ID\n"); 
		m_iConnected = CONN_OK; 
		m_nLastOrderID = orderId;
		m_oLastError.Format( "Connection established OK. Next Order Id=%d", m_nLastOrderID ); 
		if( m_poMessagesList ) m_poMessagesList->AddMessage( m_oLastError, FALSE );
	};
	virtual void contractDetails( const ContractDetails& contractDetails);
    virtual void bondContractDetails( const ContractDetails& contractDetails) {};
    virtual void execDetails( OrderId orderId, const Contract& contract, const Execution& execution) {};
	virtual void error(const int id, const int errorCode, const CString errorString);
	virtual void updateMktDepth(TickerId id, int position, int operation, int side, 
		double price, int size) {};
	virtual void updateMktDepthL2(TickerId id, int position, CString marketMaker, int operation, 
		int side, double price, int size) {};
	virtual void updateNewsBulletin(int msgId, int msgType, const CString& newsMessage, const CString& originExch) {};
    virtual void managedAccounts( const CString& accountsList);
    virtual void receiveFA(faDataType pFaDataType, const CString& cxml) {};
   virtual void historicalData(TickerId reqId, const CString& date, double open, double high, 
	   double low, double close, int volume, int barCount, double WAP, int hasGaps) {};
   virtual void scannerParameters(const CString &xml) {};
   virtual void scannerData(int reqId, int rank, const ContractDetails &contractDetails,
	   const CString &distance, const CString &benchmark, const CString &projection,
	   const CString &legsStr) {};
   virtual void scannerDataEnd(int reqId) {};
   virtual void realtimeBar(TickerId reqId, long time, double open, double high, double low, double close,
	   long volume, double wap, int count) {} ;
   virtual void currentTime(long time) {} ; public:
	   int GetExpiryFromLocalSymbol( CString oLocal, CString &oExpiry );
	BOOL IsFAAccount(const CString& account);
	CString m_oAccount;
	CString m_oInfoAccount;
	CString m_oAccountsList;
	void OrderToFAParams(Order &ord, CString &params, BOOL bFrom );
	void OrderToAttributes( Order &ord, CString &attrs, BOOL bFromOrder );
	CString ContractToTicker( Contract &contract );
	OrderId m_nLastOrderID;
	void TickerToContract( LPCTSTR pszTicker, Contract &contract );
	void ConnectLists( CBaseListCtrl **apoListCtrls );
	int m_iPeriodicity;
	CString m_oLastError;
	HWND m_hAmiWnd;
	int	 m_iConnected;

	CFIFOSymbolMap		m_oSymbols;

	CMapStringToString	m_oLocalSymbolTranslation;

	CIBAccountList		*m_poAccountList;
	CIBExecutionList	*m_poExecutionList;
	CIBMessagesList		*m_poMessagesList;
	CIBOrderList		*m_poOrderList;
	CIBPortfolioList	*m_poPortfolioList;

	CUIntArray			m_oIgnoreArray;

#ifndef NO_QUOTE_DATA
	CSymbolArray m_oSymbols;
#endif
};

#endif // !defined(AFX_WRAPPER_H__D6A4BB11_D310_4FD9_95BD_061F33E536DE__INCLUDED_)
