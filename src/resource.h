//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BrokerIB.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDS_ABOUTBOX                    101
#define IDD_BROKERIB_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDR_MAIN_MENU                   129
#define IDR_MAIN_TOOLBAR                130
#define IDD_UNLOCK_DIALOG               133
#define IDD_CONFIG_DIALOG               134
#define IDD_IGNORE_DIALOG               135
#define IDC_MAIN_TAB                    1000
#define IDC_NAME_EDIT                   1001
#define IDC_CODE_EDIT                   1002
#define IDC_CHECK1                      1003
#define IDC_URL_STATIC                  1004
#define IDC_HOST_EDIT                   1005
#define IDC_PORT_EDIT                   1006
#define IDC_MAX_ERRORS_EDIT             1007
#define IDC_IGNORE_EDIT                 1008
#define ID_ORDER_CANCEL                 32771
#define ID_ORDER_TRANSMIT               32772
#define ID_WINDOW_ALWAYS_ON_TOP         32774
#define ID_MESSAGES_CLEAR               32776
#define ID_FILE_CANCEL_ERRORNEOUS_ORDERS 32777
#define ID_ORDER_ERROR_CANCEL           32778
#define ID_FILE_ENTER_CODE              32779
#define ID_ORDER_PANIC                  32780
#define ID_FILE_CONFIG                  32781
#define ID_FILE_RECONNECT               32782
#define ID_FILE_IGNORE_LIST             32783

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
